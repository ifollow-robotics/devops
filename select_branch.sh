#!/bin/bash

DEVOPS_DEFAULT_BRANCH=$(git remote show https://gitlab.com/ifollow-robotics/devops.git | sed -n '/HEAD branch/s/.*: //p')
BRANCH_EXISTS=$(git ls-remote https://gitlab.com/ifollow-robotics/devops.git --heads origin $CI_COMMIT_REF_NAME | wc -l)
UPSTREAM_BRANCH_EXISTS=$(git ls-remote https://gitlab.com/ifollow-robotics/devops.git --heads origin $UPSTREAM_BRANCH | wc -l)
DEFAULT_BRANCHS=("onboard_dev" "master" "v4")

if [[ -n "$UPSTREAM_BRANCH" && $UPSTREAM_BRANCH_EXISTS -eq 1 ]]
then
    echo "Candidate branch name for DevOps project based based on upstream branch $UPSTREAM_BRANCH"
    # Gitlab project containing all bash scripts used by CI's
    DEVOPS_BRANCH=$UPSTREAM_BRANCH
else
    echo "Candidate branch name for DevOps project based on current branch: $CI_COMMIT_REF_NAME"
    DEVOPS_BRANCH=$CI_COMMIT_REF_NAME
fi

if [[ $BRANCH_EXISTS -eq 1 && ! ${DEFAULT_BRANCHS[*]} =~ $DEVOPS_BRANCH ]]
then
    echo "Using [$DEVOPS_BRANCH] branch of DevOps project"
else
    echo "Using [$DEVOPS_DEFAULT_BRANCH] branch of DevOps project"
    DEVOPS_BRANCH=$DEVOPS_DEFAULT_BRANCH
fi

DEVOPS_CONFIG_URL="https://gitlab.com/ifollow-robotics/devops/-/raw/$DEVOPS_BRANCH/config"
DEVOPS_SIMU_URL="https://gitlab.com/ifollow-robotics/devops/-/raw/$DEVOPS_BRANCH/simu"
