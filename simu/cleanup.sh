#!/bin/bash

GRE="\e[32;1m"
YEL="\e[33;1m"
RST="\e[0m"
RED="\e[31;1m"

function sim_cleanup()
{
    # Clear all containers, networks and volumes used by CI simulations
    # It uses the variable SIMULATION_ID (which can be a regex or a specific ID)

    docker stop $(docker ps -a | grep -E "[[:space:]]${SIMULATION_ID}-.*-[1-9]+$" | awk '{print $1}') > /dev/null 2>&1 || true
    docker rm $(docker ps -a | grep -E "[[:space:]]${SIMULATION_ID}-.*-[1-9]+$" | awk '{print $1}') > /dev/null 2>&1 || true
    docker network rm $(docker network list | grep -E "[[:space:]]${SIMULATION_ID}_[_[:alnum:]-]+[[:space:]]" | awk '{print $1}') > /dev/null 2>&1 || true
    docker volume rm $(docker volume ls | grep -E "[[:space:]]${SIMULATION_ID}_[_[:alnum:]-]+$" | awk '{print $2}') > /dev/null 2>&1 || true
}

# Init SIMULATION_ID variable with regex
ANY_CI_SIM_ID="[1-9][0-9]{4}"
SIMULATION_ID=$ANY_CI_SIM_ID

sim_ids_dir="./sim_ids"

if [ -d $sim_ids_dir ] && [ -n "$(ls -A $sim_ids_dir)" ]; then
    echo -e "${GRE}Found non-empty $sim_ids_dir directory!${RST}"
    for f in $sim_ids_dir/*.sh
    do
        # Sourcing each .sh file inside sim_ids will set a new value for SIMULATION_ID var (in theory)
        source $f

        echo "Simulation (id=$SIMULATION_ID) cleanup based on file: $f"
        sim_cleanup

        # reset SIMULATION_ID to the regex
        SIMULATION_ID=$ANY_CI_SIM_ID
    done
else
    echo -e "${YEL}Folder $sim_ids_dir does not exist or it's empty!${RST}"
    echo -e "${RED}No cleanup will be performed, relying in weekly runner cron job that uses regex (${SIMULATION_ID}) for cleaning up${RST}"
fi
