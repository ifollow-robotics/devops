#! /usr/bin/env python3
import os
import sys
from dotenv import load_dotenv

# Used to make parent folder available to import
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from config.registry_login import registry_login
from dotenv import load_dotenv
from tools.print_format import print_log, style
from tools.utils import *
from run_simulation import run_simulation
import os
import subprocess
import docker
from random import randrange
import yaml
import re
import semver
import datetime
import time

HOME = os.getcwd()


def clone_project(url, tag_over_branch=False):

    project_name = url.split('.')[-2].split('/')[-1]

    sw_image_tag = get_env_var('SW_IMAGE_TAG', show=False)

    revisions = []

    try:
        semver.Version.parse(sw_image_tag)
        revisions = [sw_image_tag]
    except ValueError as e:
        print(f'Not using SW_IMAGE_TAG, {e}')

    revisions.append(get_env_var('CI_COMMIT_BRANCH', show=False))

    if not tag_over_branch:
        revisions.reverse()

    print(f'revisions: {revisions}')

    clone_success = False

    for rev in revisions:
        print(f"{style.bBLUE}Attempt to clone {project_name} project at revision {rev}{style.RESET}")
        clone = f"git clone --depth 1 -b {rev} {url}"
        if os.system(clone) != 0:
            print(style.ORANGE +
                f"Failed to clone {project_name} project at revision {rev}" + style.RESET)
        else:
            clone_success = True
            break

    if not clone_success:
        print(style.ORANGE +
                f"Cloning {project_name} at the default branch" + style.RESET)
        clone = f"git clone --depth 1 {url}"
        if os.system(clone) != 0:
            print(style.bRED + "Failed to clone docker project!" + style.RESET)
            return

    print(style.bGREEN + f"{project_name} project cloned" + style.RESET)

def requirements_check():
    requirements = ["docker", "docker-compose", "openssl"]
    check_option = ["--version", "--version", "version"]

    for cmd, option in zip(requirements, check_option):
        print(style.bYELLOW + "Checking if " + style.bBLUE + cmd +
              style.bYELLOW + " is available:".format(cmd), end=' ')

        version = subprocess.getoutput(f'{cmd} {option}')

        if subprocess.getstatusoutput(cmd)[0] != 0:
            print(style.bRED + "NOT FOUND! Aborting...")
            sys.exit(1)
        else:
            print(style.bGREEN + "FOUND " + style.RESET + version)
    print()

def load_envs_to_sim(ci_project_name):

    # sourcing environment variables for each simulation env,
    # use override=True because ROBOT_VER from .env should take
    # precedence over the pipeline's ROBOT_VER (if any)
    # load_dotenv('.env', override=True)

    # in case of an environment without a simulation folder,
    # this override is needed
    os.environ['ROBOT_ENV'] = ci_project_name

    sim_id = str(randrange(10000, 99999))
    print("Simulation id: " + sim_id)

    # set HOME_DIR variable to the CI runner "HOME"
    os.environ['HOME_DIR'] = f'/tmp/{sim_id}'
    

def launch():

    ci_project_name = os.environ['CI_PROJECT_NAME']

    ppline_robot_ver = get_env_var('ROBOT_VER')
    ppline_env_img_tag = get_env_var('ENV_IMAGE_TAG')
    ppline_sw_img_tag = get_env_var('SW_IMAGE_TAG')

    os.mkdir("sim_logs")
    os.mkdir("test_results")
    os.mkdir("sim_ids")
    os.mkdir(f"{HOME}/.ifollow/")
    os.chdir("docker/fleet_simulation")

    load_envs_to_sim(ci_project_name)

    os.environ['ROBOT_VER'] = 'v4'
    os.environ['SW_IMAGE_TAG'] = ppline_sw_img_tag
    os.environ['SIMULATION_MODE'] = 'Stress'
    os.environ['SIMULATION_LAUNCHER'] = 'simulated_robots_stress'
    os.environ['ENV_IMAGE_TAG'] = ppline_env_img_tag

    sim = run_fleet_simulation()

    start_time = datetime.datetime.now()
    dc_logs = "docker-compose -f docker-compose.yaml logs > ../../sim_logs/{}_sim_{}.log 2>&1".format("001", "002")
    dc_down = "docker-compose -f docker-compose.yaml down -v > /dev/null 2>&1"

    while 1:
        if ((datetime.datetime.now() - start_time).seconds > 600.0):
            print(style.bRED + "Fleet simulation failed because of Timeout" + style.RESET, end='\n\n')
            os.system('docker logs fleet_simulation-simulated-robots-1')
            os.system(dc_logs)
            os.system(dc_down)
            os.chdir("..")
            sys.exit(1)
        else:
            os.system(f'docker cp $(docker ps -aqf \"name=^fleet_simulation-simulated-robots-1$\"):/home/ifollow/catkin_ws/src/simulated_robots/logs/report.txt ../../test_results')
            
            if (os.stat("../../test_results/report.txt").st_size != 0):
                print(style.bGREEN + "Fleet simulation succeeded" + style.RESET, end='\n\n')
                break  

        time.sleep(1)

    os.system(dc_logs)
    os.system(dc_down)

    os.chdir("..")


def run_fleet_simulation() -> bool:

    status = deploy_fleet_similation()

    return status

def deploy_fleet_similation() ->bool:
    print(style.bGREEN + "Pulling images for fleet simulation" +
        style.RESET, end='\n\n')
    
    dc_cmmd = "docker-compose -f docker-compose.yaml"

    # TODO: use docker python API do properly login and manage return codes from docker-compose pull
    # TODO: https://github.com/gabrieldemarmiesse/python-on-whales
    registry_login = "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    os.system(registry_login)
    exit_status = os.system(dc_cmmd + " pull")

    if exit_status != 0:
        print(style.bYELLOW +
              "Failed to pull all images, trying again without the --quiet flag...")
        if os.system(dc_cmmd + " pull") != 0:
            print(
                style.bRED + f"Failed to pull all images a second time, exit_status {exit_status}! Quitting...")
            return False

    os.system(dc_cmmd + " images")
    os.system(dc_cmmd + " ps")

    dc_up = dc_cmmd + \
        f" up -d --force-recreate "

    exit_status = os.system(dc_up + " > /dev/null 2>&1")

    if exit_status != 0:
        print(
            style.bRED +
            f"Failed to docker-compose up, exit_status {exit_status}, "
            "trying again without being quiet...")
        
        exit_status = os.system(dc_up)
        
        if exit_status != 0:
            print(
                style.bRED +
                f"Failed to docker-compose up a 2nd time, exit_status {exit_status}"
                ". Quitting...")
            return False

    return True


def main():
    print_log("Simulation setup")
    client = docker.from_env()

    requirements_check()
    clone_project("git@gitlab.com:ifollow-robotics/dev/docker.git")

    registry_login(client)

    launch()


if __name__ == "__main__":
    main()
