#! /usr/bin/env python3
import semver
from deepdiff import DeepDiff
from urllib.parse import urlparse
from tools.utils import *
from tools.print_format import print_log, style
from simu.nodes_api import get_nodes_api, get_prev_nodes_api
from simu.node_health import *
from random import sample
import json
import re
import tarfile
import time
import yaml
import socket
import docker
import os
import sys
sys.path.append('..')   # Used to make parent folder available to import

# Min value for choosing random ports:
PORTS_START = 20000
# Max value for choosing random ports:
PORTS_END = 65000
# Max duration for obtaining a success when checking nodes health:
NODES_CHECK_TIMEOUT = 300
# Max duration for obtaining a success when checking topics:
TOPICS_CHECK_TIMEOUT = 300
# After deploying the simulation, wait this amount of time before start checking its health:
INIT_DELAY = 25

dc_cmmd = "docker-compose -f docker-compose.yaml -f .docker-compose-viewer.yaml"

frames = set()
interactables = None
docking_stations = []


def is_port_in_use(port: int) -> bool:
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        return s.connect_ex(('localhost', port)) == 0


def get_available_ports(nb_ports) -> list:
    while True:
        ports = sample(range(PORTS_START, PORTS_END), k=nb_ports)

        ports_available = True
        for port in ports:
            if is_port_in_use(port):
                ports_available = False
                print("Port: " + str(port) + " not available")
                break

        if not ports_available:
            continue
        else:
            print(f"Finished choosing {nb_ports} random ports")
            return ports


def node_health_check(sim_id, nb_robots, nodes_list) -> bool:
    elapsed = 0
    start_time = time.time()
    iteration = 0

    # node_check keeps tab on what nodes have already been checked as present
    node_check = dict()

    # flag to indicate when all nodes are OK (present)
    all_nodes_ok = dict()

    for target in nodes_list.keys():

        node_check[target] = dict()
        all_nodes_ok[target] = False

        for node in nodes_list[target]['nodes']:
            node_check[target][node] = False

    while elapsed < NODES_CHECK_TIMEOUT and \
            any([not ok for ok in all_nodes_ok.values()]):

        print(style.bGREEN +
              f"Start " + style.MAGENTA + "nodes health check" + style.bGREEN +
              f" iteration #{iteration} "
              f"(elapsed time = {elapsed:.2f})" + style.RESET)

        for target in nodes_list.keys():

            if not all_nodes_ok[target]:
                print(style.bYELLOW + f"Checking {target} nodes" + style.RESET)
                for node in node_check[target]:
                    if not node_check[target][node]:
                        counter = 1
                        if target == 'robots':
                            counter = nb_robots
                        node_check[target][node] = node_health(
                            node, sim_id, nodes_list[target]['ros_master_container'], counter=counter)
                if not False in node_check[target].values():
                    all_nodes_ok[target] = True

        iteration += 1

        elapsed = time.time() - start_time

    if any([not ok for ok in all_nodes_ok.values()]):
        print_log(
            f"Nodes verification took more than {NODES_CHECK_TIMEOUT} seconds", 'error')
        for target in nodes_list.keys():
            for node, node_ok in node_check[target].items():
                if not node_ok:
                    print(f"{target} node not OK: {node}")
        return False
    else:
        return True


def deploy_sim(sim_id, nb_robots) -> bool:

    print(style.bGREEN + f"Pulling images for sim {sim_id}" +
          style.RESET, end='\n\n')

    # TODO: use docker python API do properly login and manage return codes from docker-compose pull
    # TODO: https://github.com/gabrieldemarmiesse/python-on-whales
    registry_login = "docker login -u $CI_REGISTRY_USER -p $CI_REGISTRY_PASSWORD $CI_REGISTRY"
    os.system(registry_login)
    exit_status = os.system(dc_cmmd + " pull -q")

    if exit_status != 0:
        print(style.bYELLOW +
              "Failed to pull all images, trying again without the --quiet flag...")
        if os.system(dc_cmmd + " pull") != 0:
            print(
                style.bRED + f"Failed to pull all images a second time, exit_status {exit_status}! Quitting...")
            return False

    print(style.bGREEN + f"Deploy simulation {sim_id} using docker-compose for {nb_robots} robots" +
          style.RESET, end='\n\n')

    dc_up = dc_cmmd + \
        f" up -d --force-recreate --remove-orphans --renew-anon-volumes --scale simulated-robot={nb_robots}"

    exit_status = os.system(dc_up + " > /dev/null 2>&1")
    if exit_status != 0:
        print(
            style.bRED +
            f"Failed to docker-compose up, exit_status {exit_status}, "
            "trying again without being quiet...")
        exit_status = os.system(dc_up)
        if exit_status != 0:
            print(
                style.bRED +
                f"Failed to docker-compose up a 2nd time, exit_status {exit_status}"
                ". Quitting...")
            return False

    return True


def topics_check(sim_id, nb_robots, nodes_list) -> bool:
    elapsed = 0
    start_time = time.time()
    iteration = 0

    ros_master = dict()
    ros_master['robots'] = nodes_list['robots']['ros_master_container']
    ros_master['server'] = nodes_list['server']['ros_master_container']
    ros_master['viewer'] = nodes_list['viewer']['ros_master_container']

    topics_ok = {"srv/connected_vehicles": False,
                 "robots/connected_vehicles": False,
                 "srv/segments": False,
                 "viewer/segments": False,
                 "robots/segments": False}

    while elapsed < TOPICS_CHECK_TIMEOUT and \
            any([not ok for ok in topics_ok.values()]):

        print(style.bGREEN +
              f"Start " + style.MAGENTA + "topics check" + style.bGREEN +
              f" iteration #{iteration} "
              f"(elapsed time = {elapsed:.2f})" + style.RESET)

        elapsed = time.time() - start_time

        if not topics_ok["robots/segments"]:
            print(
                f"{style.bYELLOW}Checking graph topic (/segments) in the robots{style.RESET}")
            topics_ok["robots/segments"] = check_segments_topic(
                sim_id, ros_master['robots'])

        if not topics_ok["viewer/segments"]:
            print(
                f"{style.bYELLOW}Checking graph topic (/segments) in the viewer (aka monitoring){style.RESET}")
            topics_ok["viewer/segments"] = check_segments_topic(
                sim_id, ros_master['viewer'])

        if not topics_ok["srv/segments"]:
            print(
                f"{style.bYELLOW}Checking graph topic (/segments) in the server{style.RESET}")
            topics_ok["srv/segments"] = check_segments_topic(
                sim_id, ros_master['server'])

        if not topics_ok["robots/connected_vehicles"]:
            print(
                f"{style.bYELLOW}Checking connected vehicles in the robots{style.RESET}")
            topics_ok["robots/connected_vehicles"] = check_connected_vehicles_topic(
                sim_id, ros_master['robots'], counter=nb_robots)

        if not topics_ok["srv/connected_vehicles"]:
            print(
                f"{style.bYELLOW}Checking connected vehicles in the server{style.RESET}")
            topics_ok["srv/connected_vehicles"] = check_connected_vehicles_topic(
                sim_id, ros_master['server'], counter=nb_robots)

        iteration += 1

    if any([not ok for ok in topics_ok.values()]):
        print_log(
            f"Topics verification took more than {TOPICS_CHECK_TIMEOUT} seconds", 'error')
        return False
    else:
        if is_check_supported("graph_conformity_check"):
            print(f"{style.bYELLOW}Checking graph's conformity{style.RESET}")
            return graph_conformity_check(sim_id, ros_master['server'])
        else:
            return True


def charge_mission_thru_generator_check(sim_id, nodes_list) -> bool:

    ros_master = nodes_list['server']['ros_master_container']

    container_name = sim_id + '-' + ros_master + '-1'

    client = docker.from_env(timeout=120)
    container = client.containers.get(container_name)

    bkp_current_path = os.getcwd()
    os.chdir('../../')  # we were inside gz_docker_compose/ROBOT_ENV

    # tar file needed to send to container with put_archive
    with tarfile.open('test_charge.tar', 'w') as archive:
        archive.add('devops/simu/resources/test_charge.py')

    # send tests to container
    tests = open('test_charge.tar', 'rb').read()
    container.put_archive("/home/ifollow", tests)

    os.chdir(bkp_current_path)

    print(
        f"{style.bYELLOW}Checking charge mission{style.RESET}")

    print(
        f"Checking {style.bBLUE}charge mission{style.RESET} inserted from generator: ",
        end='')

    # run tests
    cmd = "/bin/bash -c 'source /ros_entrypoint.sh && " \
          "timeout 900 ./devops/simu/resources/test_charge.py | sort -t'[' -k3n'"
    cmd_result = container.exec_run(cmd)
    output = cmd_result.output.decode('utf-8')

    if cmd_result.exit_code == 0:
        print(f"{style.bGREEN}[✓]{style.RESET}", flush=True)
        return True
    else:
        print(f"{style.bRED}[✗]{style.RESET}", flush=True)
        print(f"{style.bRED}{output}{style.RESET}", flush=True)
        return False


def run_amr_tests(sim_id, nodes_list) -> bool:
    """
    - Copy tests to gzserver container in order to have access to ros master
    - Launch tests
    - Save test_results.xml back to runner as artifact from job
    """

    print(style.bGREEN + "Integration tests" + style.RESET, end='\n\n')
    bkp_current_path = os.getcwd()
    os.chdir('../../')  # we were inside gz_docker_compose/ROBOT_ENV

    ros_master = dict()
    ros_master['robots'] = nodes_list['robots']['ros_master_container']
    robot_container_name = sim_id + '-' + ros_master['robots'] + '-1'

    client = docker.from_env()

    # tar folder needed to send to container with put_archive
    with tarfile.open('amr_tests.tar', 'w') as archive:
        archive.add('amr_tests')

    container = client.containers.get(robot_container_name)

    # send tests to container gzserver
    tests = open('amr_tests.tar', 'rb').read()
    container.put_archive("/home/ifollow", tests)

    # run tests
    print(style.bGREEN + "Running tests" + style.RESET, end='\n\n')

    # container.exec_run(['/bin/bash', '-c', 'echo $ROBOT_ENV'])

    run_tests = "/bin/bash -c 'source /ros_entrypoint.sh && pytest -svv amr_tests --junitxml=test_results.xml'"
    test_output = container.exec_run(run_tests)

    # retrive test_results as tar file
    print(style.bGREEN + "Retrieving tests results" + style.RESET, end='\n\n')

    bits, stat = container.get_archive('/home/ifollow/test_results.xml')
    with open('test_results.xml.tar', 'wb') as tar_result:
        for chunk in bits:
            tar_result.write(chunk)

    # extract tar and show results
    file_name = container.exec_run(['/bin/bash', '-c', 'echo $ROBOT_ENV'])
    file_name = file_name.output.decode().split()[0]

    with tarfile.open('test_results.xml.tar', 'r:*') as tar:
        tar.extractall('.')

    os.system(f'mv test_results.xml test_results/{file_name}.xml')

    os.chdir(bkp_current_path)

    if test_output.exit_code == 1:
        print_log("Integration tests failed", 'error')
        out_tests = False
    elif test_output.exit_code == 0:
        print(f'Integration tests have passed {style.bGREEN}[✓]{style.RESET}')
        out_tests = True
    elif test_output.exit_code == 5:
        print(f'{style.ORANGE}Integration tests skipped{style.RESET}')
        out_tests = True
    else:
        print_log("Integration tests unknown exit_code", 'error')
        return False

    return out_tests


def save_logs_and_clean(sim_id, sim_env):

    # Save logs to artifacts
    print(f"Save logs as artifacts for sim {sim_id}")

    dc_logs = dc_cmmd + " logs > ../../sim_logs/{}_sim_{}.log 2>&1".format(
        sim_env, sim_id)
    os.system(dc_logs)

    print(f"Tear down sim {sim_id}...")
    dc_down = dc_cmmd + " down -v > /dev/null 2>&1"
    os.system(dc_down)

    # Cleanup targeting this simulation ID

    cont_stop = f"docker stop $(docker ps -a " \
        f"| grep -E '[[:space:]]{sim_id}-.*-[1-9]+$' | awk '{{print $1}}') > /dev/null 2>&1"
    os.system(cont_stop)

    cont_rm = f"docker rm $(docker ps -a " \
        f"| grep -E '[[:space:]]{sim_id}-.*-[1-9]+$' | awk '{{print $1}}') > /dev/null 2>&1"
    os.system(cont_rm)

    netw_rm = f"docker network rm " \
        f"$(docker network list | grep -E '[[:space:]]{sim_id}_[_[:alnum:]-]+[[:space:]]' | awk '{{print $1}}') > /dev/null 2>&1"
    os.system(netw_rm)

    vol_rm = f"docker volume rm $(docker volume ls " \
        f"| grep -E '[[:space:]]{sim_id}_[_[:alnum:]-]+$' | awk '{{print $2}}') > /dev/null 2>&1"
    os.system(vol_rm)


def convert_to_ssh_url(url):

    if url.startswith('http'):
        parsed_url = urlparse(url)
        path_parts = parsed_url.path.strip('/').split('/')
        ssh_url = f"git@{parsed_url.netloc}:{'/'.join(path_parts)}"
        return ssh_url
    return url


def save_projects_revisions(sim_id, sim_env, nodes_list):

    print(f'Save the docker project revision (SHA) '
          f'used to deploy this pipeline (sim {sim_id})')

    docker_sha = os.popen('git rev-parse HEAD').read().rstrip()
    print(f'docker rev: {style.bBLUE}{docker_sha}{style.RESET}')

    docker_remote_url = os.popen(
        'git config --get remote.origin.url').read().rstrip()
    docker_remote_url = convert_to_ssh_url(docker_remote_url)
    print(f'docker remote url: {style.bBLUE}{docker_remote_url}{style.RESET}')

    print(f'Save the devops project revision (SHA) '
          f'used to deploy this pipeline (sim {sim_id})')

    devops_sha = os.popen(
        'git -C ../../devops rev-parse HEAD').read().rstrip()
    print(f'devops rev: {style.bBLUE}{devops_sha}{style.RESET}')

    devops_remote_url = os.popen(
        'git -C ../../devops config --get remote.origin.url').read().rstrip()
    devops_remote_url = convert_to_ssh_url(devops_remote_url)
    print(f'devops remote url: {style.bBLUE}{devops_remote_url}{style.RESET}')

    robot_container_name = sim_id + '-' + \
        nodes_list['robots']['ros_master_container'] + '-1'

    print(f'Save the generic project revision (SHA) '
          f'used by the environment image for sim {sim_id}')

    d_cmd = 'docker exec -t -e ROBOT_VER=$ROBOT_VER ' \
        f'{robot_container_name} ' \
        '/bin/bash -c "cat ~/catkin_ws/src/ifollow_common/environments/$ROBOT_VER/generic/.generic_revision_sha.txt"'

    gen_sha = os.popen(d_cmd).read().rstrip()

    d_cmd = 'docker exec -t -e ROBOT_VER=$ROBOT_VER ' \
        f'{robot_container_name} ' \
        '/bin/bash -c "git -C ~/catkin_ws/src/ifollow_common/environments/$ROBOT_VER/generic config --get remote.origin.url"'

    generic_remote_url = os.popen(d_cmd).read().rstrip()
    generic_remote_url = convert_to_ssh_url(generic_remote_url)
    print(
        f'generic remote url: {style.bBLUE}{generic_remote_url}{style.RESET}')

    if gen_sha != '' and gen_sha != '\n':
        print(f'generic rev: {style.bBLUE}{gen_sha}{style.RESET}')

        with open(f'../../generic_proj_revisions/{sim_env}.yaml', 'w') as f:
            data = {'generic': {'sha': gen_sha,
                                'env_image_tag': os.environ['ENV_IMAGE_TAG'],
                                'remote_url': generic_remote_url},
                    'docker': {'sha': docker_sha,
                               'remote_url': docker_remote_url},
                    'devops': {'sha': devops_sha,
                               'remote_url': devops_remote_url}}
            yaml.dump(data, f)

        contents = os.popen(
            'ls ../../generic_proj_revisions/').read().rstrip()
        print(f'Contents inside generic_proj_revisions:\n{contents}')
    else:
        print(style.bYELLOW + f'No generic SHA found!' + style.RESET)


def init_wait():

    print(f"Waiting {INIT_DELAY}s before starting tests...")

    for _ in range(1, INIT_DELAY):
        print(". ", end='', flush=True)
        time.sleep(1)
    print()


def set_frames(sim_id, nodes_list):

    global frames

    container_name = sim_id + '-' + \
        nodes_list['server']['ros_master_container'] + '-1'

    list_tf_static_cmd = 'docker exec -t ' \
        f'{container_name} ' \
        '/bin/bash -c "source /ros_entrypoint.sh && timeout 5 rostopic echo /tf_static"'

    static_frames = os.popen(list_tf_static_cmd).read()

    # matches frame_id and child_frame_id
    frame_re = re.compile('frame_id: "(.*)"')

    for line in static_frames.split('\n'):
        search_result = frame_re.search(line)
        if search_result:
            parts = search_result.group(1).split('/')
            frames.add(parts[len(parts) - 1].replace(
                'compound_', '').replace('_static', ''))


def set_interactables(sim_id, nodes_list):

    global interactables

    container_name = sim_id + '-' + \
        nodes_list['server']['ros_master_container'] + '-1'

    list_interac_cmd = 'docker exec -t ' \
        f'{container_name} ' \
        '/bin/bash -c "source /ros_entrypoint.sh && rosparam get /interactable_objects/definitions"'

    interactables = yaml.safe_load(os.popen(list_interac_cmd).read())


def set_docking_stations(sim_id, nodes_list) -> bool:

    global docking_stations

    container_name = sim_id + '-' + \
        nodes_list['server']['ros_master_container'] + '-1'

    list_robot_names_cmd = 'docker exec -t ' \
        f'{container_name} ' \
        '/bin/bash -c "source /ros_entrypoint.sh && rosparam get /robot_objects/definitions"'

    robot_defs = yaml.safe_load(os.popen(list_robot_names_cmd).read())

    if robot_defs:
        robot_names = [r_def['robot_name'] for r_def in robot_defs if 'robot_name' in r_def]
    else:
        # LEGACY way to get the list of robots
        print(
            f"{style.ORANGE}WARNING: It seems /robot_objects is not defined. "
            f"Trying legacy /robot_names parameter{style.RESET}")

        list_robot_names_cmd = 'docker exec -t ' \
            f'{container_name} ' \
            '/bin/bash -c "source /ros_entrypoint.sh && rosparam get /robot_names"'

        robot_names = yaml.safe_load(os.popen(list_robot_names_cmd).read())

    if not robot_names:
        print(
            f"{style.ORANGE}WARNING: It seems no robot_name is defined. Moving on...{style.RESET}")
        return True

    robot_container_name = sim_id + '-' + \
        nodes_list['robots']['ros_master_container'] + '-1'

    list_dock_controller_defs_cmd = 'docker exec -t ' \
        f'{robot_container_name} ' \
        f'/bin/bash -c "source /ros_entrypoint.sh && rosparam get ' \
        f'{robot_names[0]}/automatic_docking/dock_controller"'

    dock_controller_defs = yaml.safe_load(
        os.popen(list_dock_controller_defs_cmd).read())

    if not dock_controller_defs or 'supported_docking_stations' not in dock_controller_defs:
        print(
            f"{style.bRED}ERROR: Not found or invalid param "
            f"{robot_names[0]}/automatic_docking/dock_controller.{style.RESET}")
        docking_stations = []
        return False

    docking_stations = dock_controller_defs['supported_docking_stations']

    ds_ok = True
    for ds in docking_stations:
        if ds not in dock_controller_defs:
            print(f'{style.bRED}dock station '
                  f'{style.bBLUE}{ds}{style.RESET} '
                  f'{style.bRED}couldn\'t be found among automatic_docking/dock_controller definitions [✗]\n'
                  f'Check your docking parameters{style.RESET}')
            ds_ok = False

    return ds_ok


def interac_obstacle_labels_check():

    print(f"{style.bYELLOW}Checking interactables' obstacle_labels{style.RESET}")

    global interactables

    if not interactables:
        print(
            f"{style.ORANGE}WARNING: It seems no interactable is defined. Moving on...{style.RESET}")
        return True

    obst_wrt_dock_ok = True
    if not docking_stations:
        print(
            f"{style.ORANGE}WARNING: It seems no docking stations are defined.{style.RESET}")

    obst_wrt_frames_ok = True

    for interac_name, interac_def in interactables.items():

        if not isinstance(interac_def, dict) or 'obstacle_label' not in interac_def:
            print(f"{style.ORANGE}WARNING: 'interactables' value: {interactables}{style.RESET}")
            print(
                f"{style.ORANGE}WARNING: It seems no interactable is defined. Moving on...{style.RESET}")
            return True

        obst_label = interac_def['obstacle_label']
        if obst_label != "":
            if obst_label not in frames:
                print(f'{style.bRED}{interac_name}\'s obstacle_label '
                      f"{style.bBLUE}{obst_label}{style.RESET} "
                      f'{style.bRED}couldn\'t be found among {style.bBLUE}frames{style.RESET}{style.bRED} [✗]\n'
                      'If this is a frame-less interactable (no docking) '
                      f'you have to leave the obstacle_label field empty!{style.RESET}')
                obst_wrt_frames_ok = False
            if docking_stations != [] and obst_label.split('_')[0] not in docking_stations:
                print(f'{style.bRED}{interac_name}\'s obstacle_label '
                      f"{style.bBLUE}{obst_label}{style.RESET} "
                      f'{style.bRED}couldn\'t be found among {style.bBLUE}docking_stations{style.RESET}{style.bRED} [✗]\n'
                      'If this is a frame-less interactable (no docking) '
                      f'you have to leave the obstacle_label field empty!{style.RESET}')
                obst_wrt_dock_ok = False

    if obst_wrt_frames_ok and obst_wrt_dock_ok:
        print(f"All interactables' {style.bBLUE}obstacle_labels{style.RESET} "
              f"are valid {style.bGREEN}[✓]{style.RESET}")

    return obst_wrt_frames_ok and obst_wrt_dock_ok


def nodes_api_check(sim_id, sim_env, nodes_list) -> bool:

    print(f"{style.bYELLOW}Retrieving current ROS nodes API...{style.RESET}")

    # Retrieve nodes API
    nodes_api = get_nodes_api(sim_id, nodes_list)

    # Save nodes_api to artifact file
    with open(f'../../nodes_api/{sim_env}/api.yaml', 'w') as fp:
        yaml.dump(nodes_api, fp, default_flow_style=False)

    # Download the artifacts of the latest successful pipeline
    status_code, content = get_prev_nodes_api(sim_env, rootdir='../../')

    if status_code == 200:
        # Load the yaml
        prev_nodes_api = yaml.safe_load(content.decode("utf-8"))

        ddiff = DeepDiff(prev_nodes_api, nodes_api, ignore_order=True)

        if ddiff == {}:
            # All good, no API change
            print(f'There has been no change in the {style.bBLUE}ROS nodes API '
                  f'{style.bGREEN}[✓]{style.RESET}')
            return True
        else:
            # Show the api changes in the logs
            print(
                f"{style.bBLUE}ROS nodes API{style.RESET} has changed! {style.ORANGE}[!]")
            print(json.dumps(json.loads(ddiff.to_json()), indent=4))
            print(f"{style.RESET}")

            # Save diff to file in artifacts
            with open(f'../../nodes_api/{sim_env}/ddiff.json', 'w') as fp:
                fp.write(f'{ddiff.to_json()}')

            return False
    else:
        print(f"{style.bRED}Download failed with code: {status_code}{style.RESET}")
        return True


def init_check_requirements(checks_req):

    global g_checks_req
    g_checks_req = checks_req

    global g_sw_ver
    try:
        g_sw_ver = semver.Version.parse(
            get_env_var('SW_IMAGE_TAG', show=False))
    except ValueError as e:
        print(
            f'Error parsing version, {e}')
        g_sw_ver = None


def is_check_supported(check_name: str):

    if g_sw_ver == None:
        # assuming supported
        return True

    if check_name not in g_checks_req:
        # unknown check, assuming unsupported
        return False

    if g_checks_req[check_name] == []:
        # no spec on version, assuming supported
        return True

    for spec in g_checks_req[check_name]:
        op = spec[0]
        ver_str = spec[1]
        try:
            req_ver = semver.Version.parse(ver_str)
        except ValueError as e:
            print(
                f'Error parsing version {ver_str}: {e}')
            break

        if g_sw_ver < req_ver:
            if op != '<' and op != '<=':
                break
        elif g_sw_ver == req_ver:
            if op != '==' and op != '>=' and op != '<=':
                break
        else:  # g_sw_ver > req_ver
            if op != '>' and op == '>=':
                break
    else:  # no break was called, assuming supported
        return True

    # assuming unsupported
    return False


class status():
    OK = 0
    FAILURE = 1
    API_WARNNIG = 2


def run_simulation(sim_id, sim_env, nb_robots: int, checks_req={}):

    print_log("Running simulation for \33[1m\33[4m" + sim_env)

    init_check_requirements(checks_req)

    PORTS_LIST = []

    with open('.env', 'r') as dot_env_file:
        for line in dot_env_file:
            parts = line.split('=')
            if '_PORT' in parts[0]:
                PORTS_LIST.append(parts[0])

    ports = get_available_ports(len(PORTS_LIST))

    for idx, env_var in enumerate(PORTS_LIST):
        os.environ[env_var] = str(ports[idx])

    # simulation will be deployed in a single machine
    os.environ['NET_DRIVER'] = "bridge"
    os.environ['SIMULATION_ID'] = sim_id

    # Set COMPOSE_PROJECT_NAME with the simulation ID
    os.environ['COMPOSE_PROJECT_NAME'] = sim_id

    if not deploy_sim(sim_id, nb_robots):
        save_logs_and_clean(sim_id, sim_env)
        return status.FAILURE

    # load yaml file containing nodes to be verified
    with open('../../devops/simu/resources/nodes_list.yaml', 'r') as nodes:
        nodes_list = yaml.safe_load(nodes)

    init_wait()

    if is_check_supported("interac_obstacle_labels_check"):
        set_frames(sim_id, nodes_list)
        set_interactables(sim_id, nodes_list)
        if not set_docking_stations(sim_id, nodes_list):
            save_logs_and_clean(sim_id, sim_env)
            return status.FAILURE

        if not interac_obstacle_labels_check():
            save_logs_and_clean(sim_id, sim_env)
            return status.FAILURE

    if is_check_supported("node_health_check"):
        if not node_health_check(sim_id, nb_robots, nodes_list):
            save_logs_and_clean(sim_id, sim_env)
            return status.FAILURE

    if is_check_supported("topics_check"):
        if not topics_check(sim_id, nb_robots, nodes_list):
            save_logs_and_clean(sim_id, sim_env)
            return status.FAILURE

    if is_check_supported("nodes_api_check"):
        api_ok = nodes_api_check(sim_id, sim_env, nodes_list)
    else:
        api_ok = True

    if is_check_supported("charge_mission_thru_generator_check"):
        if not charge_mission_thru_generator_check(sim_id, nodes_list):
            save_logs_and_clean(sim_id, sim_env)
            return status.FAILURE

    if not run_amr_tests(sim_id, nodes_list):
        save_logs_and_clean(sim_id, sim_env)
        return status.FAILURE

    save_projects_revisions(sim_id, sim_env, nodes_list)
    save_logs_and_clean(sim_id, sim_env)

    return status.OK if api_ok else status.API_WARNNIG


def main():
    pass


if __name__ == "__main__":
    main()
