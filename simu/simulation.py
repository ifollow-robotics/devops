#! /usr/bin/env python3
import os
import sys
# Used to make parent folder available to import
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from config.registry_login import registry_login
from dotenv import load_dotenv
from tools.print_format import print_log, style
from tools.utils import *
from run_simulation import run_simulation, status
import os
import subprocess
import docker
from random import randrange
import yaml
import re
import semver
import requirements

HOME = os.getcwd()


def clone_project(url, tag_over_branch=False):

    project_name = url.split('.')[-2].split('/')[-1]

    sw_image_tag = get_env_var('SW_IMAGE_TAG', show=False)

    revisions = []

    try:
        semver.Version.parse(sw_image_tag)
        revisions = [sw_image_tag]
    except ValueError as e:
        print(
            f'[clone_project] Not using SW_IMAGE_TAG in the list of revisions, {e}')

    revisions.append(get_env_var('CI_COMMIT_BRANCH', show=False))

    if not tag_over_branch:
        revisions.reverse()

    print(f'revisions: {revisions}')

    clone_success = False

    for rev in revisions:
        print(
            f"{style.bBLUE}Attempt to clone {project_name} project at revision {rev}{style.RESET}")
        clone = f"git clone --depth 1 -b {rev} {url}"
        if os.system(clone) != 0:
            print(style.ORANGE +
                  f"Failed to clone {project_name} project at revision {rev}" + style.RESET)
        else:
            clone_success = True
            break

    if not clone_success:
        print(style.ORANGE +
              f"Cloning {project_name} at the default branch" + style.RESET)
        clone = f"git clone --depth 1 {url}"
        if os.system(clone) != 0:
            print(style.bRED + "Failed to clone docker project!" + style.RESET)
            return

    print(style.bGREEN + f"{project_name} project cloned" + style.RESET)

    # Log the current commit hash
    os.chdir(project_name)
    current_commit = os.popen('git rev-parse HEAD').read().strip()
    print(style.bGREEN + f"Current commit: {current_commit}" + style.RESET)
    os.chdir('..')

def create_certificates():

    print(f"HOME: {HOME}")

    os.chdir("crossbar")

    print(f"{style.bBLUE}Creating certificates...{style.RESET}")
    os.system("./create-certificates.sh 1 > /dev/null 2>&1")

    try:
        os.mkdir(f"{HOME}/.ifollow/")
    except FileExistsError as err:
        print(f"{style.ORANGE}Failed invoking os.mkdir: {err}. Moving on{style.RESET}")

    try:
        os.mkdir(f"{HOME}/.ifollow/crossbar/")
    except FileExistsError as err:
        print(f"{style.ORANGE}Failed invoking os.mkdir: {err}. Moving on{style.RESET}")

    try:
        os.mkdir(f"{HOME}/.ifollow/cert/")
    except FileExistsError as err:
        print(f"{style.ORANGE}Failed invoking os.mkdir: {err}. Moving on{style.RESET}")

    os.rename(".certs/client0.crt",
              f"{HOME}/.ifollow/cert/wamp_client.crt")

    os.rename(".certs/client0.key",
              f"{HOME}/.ifollow/cert/wamp_client.key")

    os.rename(".certs/server.key",
              f"{HOME}/.ifollow/crossbar/server.key")

    os.rename(".certs/server.crt",
              f"{HOME}/.ifollow/crossbar/server.crt")

    os.rename(".certs/ca.cert.pem",
              f"{HOME}/.ifollow/crossbar/ca.cert.pem")

    os.rename(".certs/intermediate.cert.pem",
              f"{HOME}/.ifollow/crossbar/intermediate.cert.pem")

    os.rename(".certs/dhparam",
              f"{HOME}/.ifollow/crossbar/dhparam")

    os.popen(f'cp config.json {HOME}/.ifollow/crossbar/')

    fingerprint = os.popen(
        f"openssl x509 -noout -in {HOME}/.ifollow/cert/wamp_client.crt -fingerprint -sha1").read().split('=')[1].strip()

    print(f'wamp_client certificate\'s fingerprint: {fingerprint}')

    input_file = "config_tls_and_no_auth_1_worker.json"
    output_file = f"{HOME}/.ifollow/crossbar/{input_file}"

    with open(input_file, 'r') as input_file, open(output_file, 'w') as output_file:
        for line in input_file:
            # Use a regular expression to match the line with "certificate-sha1"
            if re.search(r'"certificate-sha1": ".*"', line):
                # Replace the line with the new pattern
                line = re.sub(r'"certificate-sha1": ".*"',
                              f'"certificate-sha1": "{fingerprint}"', line)
            # Write the line to the output file
            output_file.write(line)

    # crossbar user ID is 242
    os.system(f'chown -R 242 {HOME}/.ifollow/crossbar')

    os.chdir("..")


def dependencies_check():
    dependencies = ["docker", "docker-compose", "openssl"]
    check_option = ["--version", "--version", "version"]

    for cmd, option in zip(dependencies, check_option):
        print(style.bYELLOW + "Checking if " + style.bBLUE + cmd +
              style.bYELLOW + " is available:".format(cmd), end=' ')

        version = subprocess.getoutput(f'{cmd} {option}')

        if subprocess.getstatusoutput(cmd)[0] != 0:
            print(style.bRED + "NOT FOUND! Aborting...")
            sys.exit(1)
        else:
            print(style.bGREEN + "FOUND " + style.RESET + version)
    print()


def tag_image_further_test(client):
    # tag the image so it can be used by the docker-compose services

    # ci_application_tag = os.environ['CI_APPLICATION_TAG']
    ci_repository = os.environ['CI_APPLICATION_REPOSITORY']
    pipeline_id = os.environ['CI_PIPELINE_ID']
    ci_registry_img = os.environ['CI_REGISTRY_IMAGE']
    ci_commit_ref_name = os.environ['CI_COMMIT_REF_NAME']

    print(f"CI_APPLICATION_REPOSITORY: {ci_repository}")
    print(f"CI_PIPELINE_ID: {pipeline_id}")
    print(f"CI_REGISTRY_IMAGE: {ci_registry_img}")
    print(f"CI_COMMIT_REF_NAME: {ci_commit_ref_name}")

    client.api.pull(ci_repository + ':' + pipeline_id)
    client.api.tag(ci_repository + ':' + pipeline_id,
                   ci_registry_img + ':' + ci_commit_ref_name)


def load_envs_to_sim(scope, simulable_envs, ci_project_name):

    # self scope means that this is an environment project in it self
    if scope == 'self':
        if ci_project_name != "environment":
            envs_to_sim_from_scope = [ci_project_name]
        else:
            envs_to_sim_from_scope = []
    elif scope != None:
        # load a yaml file named with the scope value
        try:
            with open(f'../devops/simu/resources/envs_to_sim_scopes/{scope}.yaml',
                      'r') as f:
                envs_to_sim_from_scope = yaml.safe_load(f)['envs']
        except Exception as e:
            print(style.bRED + f"Failed to open the file: {e}" + style.RESET)
            print(
                f"Contests of envs_to_sim_scopes dir: "
                f"{os.listdir('../devops/simu/resources/envs_to_sim_scopes')}")
            # if laoding from file fails, use largest scope (aka, all simulable_envs)
            envs_to_sim_from_scope = simulable_envs
    else:
        envs_to_sim_from_scope = simulable_envs

    print(f"Scope of envs to simulate: {envs_to_sim_from_scope}")

    return envs_to_sim_from_scope


def load_checks_requirements():
    checks_req = dict()
    with open(f'../devops/simu/resources/checks_requirements.txt', 'r') as fd:
        for check in requirements.parse(fd):
            checks_req[check.name] = check.specs
    return checks_req


def copy_crossbar_conf_to_host():

    pipeline_id = os.environ['CI_PIPELINE_ID']

    os.system(
        f'docker run --rm -d --name certs-cont-{pipeline_id} -v /tmp/{pipeline_id}/.ifollow:/tmp alpine sleep 7200')

    os.system(
        f'docker cp {HOME}/.ifollow/crossbar certs-cont-{pipeline_id}:/tmp/')

    os.system(
        f'docker stop certs-cont-{pipeline_id}')


def copy_wamp_client_to_host(env):

    pipeline_id = os.environ['CI_PIPELINE_ID']

    os.system(
        f'cp {HOME}/.ifollow/cert/wamp_client.crt {HOME}/.ifollow/cert/{env}/')

    os.system(
        f'cp {HOME}/.ifollow/cert/wamp_client.key {HOME}/.ifollow/cert/{env}/')

    os.system(
        f'docker run --rm -d --name certs-cont-{pipeline_id} -v /tmp/{pipeline_id}/.ifollow:/tmp alpine sleep 7200')

    os.system(
        f'docker cp {HOME}/.ifollow/cert certs-cont-{pipeline_id}:/tmp/')

    os.system(
        f'docker stop certs-cont-{pipeline_id}')


def get_number_of_robots():

    nb_robots = os.environ['NB_ROBOTS']
    if nb_robots == "":
        print(style.bRED + "No NB_ROBOTS defined for  " +
              style.bBLUE + os.environ['ROBOT_ENV'] + style.RESET)
        nb_robots = 2

    clone_project(
        f"git@gitlab.com:ifollow-robotics/common/envs/{os.environ['ROBOT_ENV']}.git")

    robot_list_path = f"{os.environ['ROBOT_ENV']}/params/interactables/robot_list.yaml"

    robot_names = []

    if os.path.exists(robot_list_path):
        with open(robot_list_path, "r") as stream:
            robot_list = yaml.safe_load(stream)
            if "robot_objects" in robot_list and \
                    "definitions" in robot_list["robot_objects"]:
                for definiton in robot_list["robot_objects"]["definitions"]:
                    if "robot_name" in definiton:
                        robot_names.append(definiton["robot_name"])

    if robot_names != []:
        return min(len(robot_names), int(nb_robots))
    else:
        return int(nb_robots)


def launch():

    ci_project_name = os.environ['CI_PROJECT_NAME']

    ppline_robot_ver = get_env_var('ROBOT_VER')
    ppline_env_img_tag = get_env_var('ENV_IMAGE_TAG')
    ppline_sw_img_tag = get_env_var('SW_IMAGE_TAG')

    os.mkdir('sim_logs')
    os.mkdir('test_results')
    os.mkdir('sim_ids')
    os.mkdir('generic_proj_revisions')
    os.mkdir('nodes_api')

    os.chdir('gz_docker_compose')

    # Get all simulable environments
    simulable_envs = [env for env in os.listdir()
                      if env != 'generic' and env != 'common']

    simulable_envs.sort()

    scope = get_env_var('ENVS_TO_SIM_SCOPE')

    envs_to_sim_from_scope = load_envs_to_sim(scope,
                                              simulable_envs, ci_project_name)

    checks_req = load_checks_requirements()

    pipeline_id = os.environ['CI_PIPELINE_ID']

    copy_crossbar_conf_to_host()

    for env in envs_to_sim_from_scope:

        if env not in simulable_envs:
            os.chdir('generic')
        else:
            os.chdir(env)

        print(style.bYELLOW + "Visiting " + style.bBLUE +
              env + style.bYELLOW + " environment" + style.RESET)

        # sourcing environment variables for each simulation env,
        # use override=True because ROBOT_VER from .env should take
        # precedence over the pipeline's ROBOT_VER (if any)
        load_dotenv('.env', override=True)

        # in case of an environment without a simulation folder,
        # this override is needed
        os.environ['ROBOT_ENV'] = env

        # set HOME_DIR variable to the CI runner "HOME"
        os.environ['HOME_DIR'] = f'/tmp/{pipeline_id}'

        dckr_robot_ver = os.environ['ROBOT_VER']
        if dckr_robot_ver != "":
            print(style.bYELLOW + "Using ROBOT_VER = " + style.bBLUE +
                  dckr_robot_ver + style.bYELLOW + " for this environment" + style.RESET)

        nb_robots = get_number_of_robots()

        #     pipeline_rob_ver matches ROBOT_VER from .env => run the simulation
        if (ppline_robot_ver == None and dckr_robot_ver != "v3") or (dckr_robot_ver == ppline_robot_ver):
            print(style.bGREEN + "Calling run_simulation for " + env +
                  " environment with " + str(nb_robots) + " robots" + style.RESET)

            os.mkdir(f'../../nodes_api/{env}')

            sim_id = str(randrange(10000, 99999))
            print(f'Simulation id: {sim_id}')

            # Dump sim_id to a file so we can use it in after_script
            with open(f'../../sim_ids/{env}.sh', 'w') as f:
                f.write(f'SIMULATION_ID={sim_id}')

            # Restore ppline_env_img_tag and ppline_sw_img_tag if valid (it got overriden by load_dotenv)
            if ppline_env_img_tag:
                os.environ['ENV_IMAGE_TAG'] = ppline_env_img_tag

            if ppline_sw_img_tag:
                os.environ['SW_IMAGE_TAG'] = ppline_sw_img_tag

            # Copy certificates
            try:
                os.mkdir(f"{HOME}/.ifollow/cert/{env}")
            except FileExistsError as err:
                print(
                    f"{style.ORANGE}Failed invoking os.mkdir: {err}. Moving on{style.RESET}")

            copy_wamp_client_to_host(env)

            sim = run_simulation(sim_id, env, nb_robots, checks_req)

            if sim == status.OK:
                print_log(f'OK for environment {env}', 'success')
            elif sim == status.FAILURE:
                print_log(f'Failed for the environment {env}', 'error')
                if ppline_robot_ver != None:
                    os.environ['ROBOT_VER'] = ppline_robot_ver
                sys.exit(1)
            elif sim == status.API_WARNNIG:
                print_log(
                    f'OK for environment {env}, but there has been an API change', 'success')

        os.chdir("..")

        if ppline_robot_ver != None:
            os.environ['ROBOT_VER'] = ppline_robot_ver

    # Save pipeline data to artifact
    pipeline_job_data = {'pipeline_id': pipeline_id,
                         'job_id': os.environ['CI_JOB_ID']}
    with open(f'../nodes_api/pipeline_job_data.yaml', 'w') as fp:
        yaml.dump(pipeline_job_data, fp, default_flow_style=False)


def main():
    print_log("Simulation setup")
    client = docker.from_env()

    dependencies_check()
    clone_project("git@gitlab.com:ifollow-robotics/DevOps_Tools/gz_docker_compose.git",
                  tag_over_branch=True)
    clone_project("git@gitlab.com:ifollow-robotics/mas/crossbar.git")
    create_certificates()
    registry_login(client)

    launch()


if __name__ == "__main__":
    main()
