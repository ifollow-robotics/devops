#!/usr/bin/python3
from sys import argv
from time import sleep
import docker
import yaml

from tools.print_format import style


def ros_cmd(cmd, extra_args=""):
    base = "/bin/bash -c 'source /ros_entrypoint.sh && "
    return base + cmd + extra_args


def get_topic_list(container, partial_topic_name):

    cmd = f"/bin/bash -c 'source /ros_entrypoint.sh && rostopic list | grep {partial_topic_name}'"

    byte_list = container.exec_run(cmd).output

    return byte_list.decode('utf-8').split()


def check_segments_topic(sim_id, ros_master):
    # the segment topic is actually the navigation graph

    container_name = sim_id + '-' + ros_master + '-1'

    client = docker.from_env(timeout=120)
    container = client.containers.get(container_name)

    topic_list = get_topic_list(container, "/segments")

    for tp in topic_list:
        print(f"Checking if topic {style.bBLUE}{tp}{style.RESET} returns "
              "a valid list: ", end='')

        cmd = f"/bin/bash -c 'source /ros_entrypoint.sh && " \
              f"timeout 5 rostopic echo -n 1 {tp}'"

        byte_list = container.exec_run(cmd).output

        try:
            ros_msg = byte_list.decode('utf-8')

            # get the first (and only) document in the ros_msg
            graph = next(yaml.safe_load_all(ros_msg))

            nb_vertices = len(graph["vertices"])

            if nb_vertices > 1:
                print(
                    f"{style.bGREEN}found {nb_vertices} nodes in the graph [✓]{style.RESET}", flush=True)
            else:
                print(
                    f"{style.bRED}found {nb_vertices} nodes in the graph [✗]{style.RESET}", flush=True)
                return False

        except Exception as e:
            print(f"Failed to read the segments topics: {e}")
            return False

    return True


def check_connected_vehicles_topic(sim_id, ros_master, counter):

    container_name = sim_id + '-' + ros_master + '-1'

    client = docker.from_env(timeout=120)
    container = client.containers.get(container_name)

    topic_list = get_topic_list(container, "connected_vehicles")

    for tp in topic_list:
        print(f"Checking if topic {style.bBLUE}{tp}{style.RESET} returns "
              f"{counter} connected vehicle(s): ", end='')

        cmd = f"/bin/bash -c 'source /ros_entrypoint.sh && " \
              f"timeout 5 rostopic echo -n 1 {tp}/vehicles_id | head -1'"

        byte_list = container.exec_run(cmd).output

        try:
            out = eval(byte_list.decode('utf-8').split('\n')[0])

            if len(out) == int(counter):
                print(f"{style.bGREEN}[✓]{style.RESET}", flush=True)
            else:
                print(f"{style.bRED}[✗]{style.RESET}", flush=True)
                return False

        except Exception as e:
            print("List of connected vehicles is not available: {}".format(e))
            return False

    return True


def graph_conformity_check(sim_id, ros_master):

    container_name = sim_id + '-' + ros_master + '-1'

    client = docker.from_env(timeout=120)
    container = client.containers.get(container_name)

    topic_list = get_topic_list(container, "/segments")

    for tp in topic_list:
        print(
            f"Checking graph's conformity from topic {style.bBLUE}{tp}{style.RESET}: ",
            end='')

        cmd = f"/bin/bash -c 'source /ros_entrypoint.sh && " \
              f"timeout 60 rosrun graph2d graph_conformity_node _topic:={tp}'"

        cmd_result = container.exec_run(cmd)

        err_msg = cmd_result.output.decode('utf-8')

        if cmd_result.exit_code == 0:
            print(f"{style.bGREEN}[✓]{style.RESET}", flush=True)
            print(err_msg, flush=True)
            return True
        else:
            print(f"{style.bRED}[✗]{style.RESET}", flush=True)
            print(f"{style.bRED}{err_msg}{style.RESET}", flush=True)
            return False

    return True


def node_health(node, sim_id, ros_master, counter=1) -> bool:

    container_name = sim_id + '-' + ros_master + '-1'

    client = docker.from_env()
    container = client.containers.get(container_name)

    cmd = f"/bin/bash -c 'source /ros_entrypoint.sh && rosnode list | grep {node}$'"

    byte_list = container.exec_run(cmd).output

    nodes_list = byte_list.decode('utf-8').split()

    # if the desired node does not even appear on the list of nodes...
    if len(nodes_list) != int(counter):
        print("Node to be checked " + style.bBLUE + f"{node}" + style.RESET +
              f" appears {len(nodes_list)} times in \"rosnode list\" output, expect {counter}: " +
              style.bRED + "[✗]" + style.RESET)

    nb_ok_nodes = 0
    for nd in nodes_list:
        print("Checking if node " + style.bBLUE + "{}".format(nd) +
              style.RESET + " is working: ", end='', flush=True)
        cmd = "/bin/bash -c 'source /ros_entrypoint.sh && rosnode info {} &>/dev/null'".format(
            nd)
        out = container.exec_run(cmd)

        if out.exit_code == 0:
            print(f"{style.bGREEN}[✓]{style.RESET}", flush=True)
            nb_ok_nodes += 1
        else:
            print(f"{style.bRED}[✗]{style.RESET}", flush=True)

    if len(nodes_list) == counter and int(counter) == nb_ok_nodes:
        return True
    else:
        return False


if __name__ == "__main__":
    node_health(argv)
