#!/usr/bin/python3

import rospy
import math
import sys
import struct
from ifollow_utils import id_ops
from std_msgs.msg import UInt64MultiArray
from ifollow_ll_msgs.msg import RobotStatus
from ifollow_mas_msgs.msg import ConnectedVehicles
from diagnostic_msgs.srv import SelfTest
from std_srvs.srv import SetBool, SetBoolRequest, Empty
from ifollow_srv_msgs.srv import SetString, SetStringRequest, SetStringResponse, GetUInt64MultiArray, GetUInt64MultiArrayRequest
from ifollow_srv_msgs.srv import SetUInt64, SetUInt64Response, SetUInt64Request
from ifollow_nav_msgs.msg import AbsolutePose2DStamped
from gazebo_msgs.srv import SetModelState, SetModelStateRequest
from ifollow_mas_msgs.srv import AddBridge, AddBridgeRequest
from ifollow_mas_msgs.msg import BridgeDescriptor
from time import sleep


class SrvStr:
    abort_macro_action = '/ma_system/action_runner/abort_macro_action'
    reset_robot = '/ma_system/generators/manager/cancel_robot'
    get_macro_action_objects = '/ma_system/action_runner/get_macro_action_objects'
    gen_diag = '/ma_system/generators/manager/diag'
    pclt_add_bridge = "public_client/add_bridge"
    set_model_state = 'simulator/set_model_state'


class Timeout:
    macro_action = rospy.Duration(100)
    charging_state = rospy.Duration(15)
    navigation = rospy.Duration(200)
    initialization = rospy.Duration(10)
    assigned = rospy.Duration(50)
    cancelation = rospy.Duration(15)


class States:

    def __init__(self):
        self.mode = ""
        self.robot = ""
        self.charging = ""
        self.assigned_interac = ""
        self.is_available = None

    def copy_from_status(self, status):
        self.charging = status.sm_docking
        self.mode = status.sm_mode
        self.robot = status.sm_robot
        self.assigned_interac = status.assigned_interactable
        self.is_available = status.is_available


class AMR:

    def __init__(self, robot_id: str):

        self.states = States()
        self.init_states = States()

        self.robot_name = id_ops.decodeId(robot_id)

        self.robot_status_sub = rospy.Subscriber("/" + self.robot_name + "/robot_status",
                                                 RobotStatus, self.on_robot_status)

        self.pose_sub = rospy.Subscriber("/" + self.robot_name + "/pose2d",
                                         AbsolutePose2DStamped, self.on_pose)

        self.pose = None
        self.init_pose = None

    def is_init(self):
        if self.init_pose != None and self.init_states.is_available != None:
            return True
        return False

    def on_robot_status(self, msg):
        if self.states.is_available == None:
            self.init_states.copy_from_status(msg)
        self.states.copy_from_status(msg)

    def on_pose(self, msg):
        if not self.pose:
            self.init_pose = msg
        self.pose = msg

    def reset_pose(self, pose=None):

        new_pose = pose if pose != None else self.init_pose

        if new_pose == None:
            return

        # TODO: get world to simulator frame and transform pose
        req = SetModelStateRequest()
        req.model_state.model_name = self.robot_name
        req.model_state.pose.position.x = new_pose.x
        req.model_state.pose.position.y = new_pose.y
        req.model_state.pose.position.z = 0.2
        req.model_state.pose.orientation.z = math.sin(
            new_pose.theta / 2.0)
        req.model_state.pose.orientation.w = math.cos(
            new_pose.theta / 2.0)

        resp = rospy.ServiceProxy(SrvStr.set_model_state, SetModelState)(req)

        rospy.loginfo(f"Call to service {SrvStr.set_model_state} returned "
                      f"success '{resp.success}' and message '{resp.status_message}'")

        rospy.ServiceProxy(self.robot_name +
                           '/set_initpose_from_sim_ground_truth', Empty)()


class SystemObserver:

    def __init__(self):
        """
        """

        self.amrs = dict()

        self.connected_vehicles = list()

        self.generators_for_robot = dict()

        self.enabled_gens = set()

        self.macro_action_agents = dict()
        self.active_macro_actions = dict()
        self.active_macro_actions_sub = rospy.Subscriber("/ma_system/action_runner/active_macro_actions",
                                                         UInt64MultiArray, self.on_active_macro_actions)

        self.connected_vehicles_sub = rospy.Subscriber("/connected_vehicles",
                                                       ConnectedVehicles, self.on_connected_vehicles)

    def listen_to_amr(self, robot_id: str) -> bool:
        """ Add an AMR to listen to

        Args:
            robot_id (str): AMR ID, ex.: '4029'
        """

        if robot_id in self.amrs:
            return False

        self.amrs[robot_id] = AMR(robot_id)
        return True

    def on_active_macro_actions(self, msg):
        """ Callback function for the active macro actions topic

        Args:
            msg (UInt64MultiArray): ROS message containing the active macro actions
        """
        # check data sanity
        if len(msg.layout.dim) != 3:
            rospy.logerr(
                "Received macro action active topic with wrong dimensions")
            return
        nb_ma = msg.layout.dim[0].size
        if msg.layout.dim[1].size != nb_ma:
            rospy.logerr(
                "Received macro action active topic with wrong dimensions")
            return
        if msg.layout.dim[2].size != nb_ma:
            rospy.logerr(
                "Received macro action active topic with wrong dimensions")
            return
        if len(msg.data) != 3*nb_ma:
            rospy.logerr(
                "Received macro action active topic with wrong dimensions")
            return

        self.active_macro_actions.clear()
        for i in range(nb_ma):

            ma_id = msg.data[3*i]

            self.active_macro_actions[ma_id] = {
                'type': msg.data[3*i+1], 'status': msg.data[3*i+2]}

            # Pack/Unpack format caractabers:
            # https://docs.python.org/3/library/struct.html#format-characters
            # It explains strings "bBBbHH" and "Q"

            # predecoding status
            data_status = struct.unpack(
                'bBBbHH', struct.pack('Q', msg.data[3*i+2]))
            self.active_macro_actions[ma_id]['ma_state'] = data_status[0]
            self.active_macro_actions[ma_id]['going_to_sleep'] = (
                data_status[1] & 0b10000000) > 0
            self.active_macro_actions[ma_id]['nb_steps'] = data_status[1] & 0b01111111
            self.active_macro_actions[ma_id]['step_idx'] = data_status[2]
            self.active_macro_actions[ma_id]['step_state'] = data_status[3]
            self.active_macro_actions[ma_id]['entry'] = data_status[4]
            self.active_macro_actions[ma_id]['exit'] = data_status[5]

            # check if macro action objects are retrieved
            if ma_id not in self.macro_action_agents:
                self.get_macro_action_objects(ma_id)

    def get_macro_action_objects(self, ma_id: int):
        """ Get the list of robots and interactables involved in a macro action

        Args:
            ma_id (int): ID of the macro action
        """
        # rospy.loginfo("get_macro_action_objects for ma id {}".format(ma_id))
        rospy.wait_for_service(SrvStr.get_macro_action_objects)
        try:
            req = GetUInt64MultiArrayRequest()
            req.id = ma_id
            resp = rospy.ServiceProxy(
                SrvStr.get_macro_action_objects, GetUInt64MultiArray)(req)
            if not resp.success:
                rospy.logerr(
                    "Service call abort macro action did not succeed : {}".format(resp.message))
            else:
                nb_robots = resp.data.layout.dim[0].size
                nb_interac = resp.data.layout.dim[1].size
                robot_list = []
                interac_list = []
                if (len(resp.data.data) == nb_robots+nb_interac):
                    for i in range(nb_robots):
                        robot_list.append(resp.data.data[i])
                    for i in range(nb_interac):
                        interac_list.append(resp.data.data[i+len(robot_list)])
                    self.macro_action_agents[ma_id] = {
                        "robots": robot_list, "interactables": interac_list}
                else:
                    rospy.logerr(
                        "Dimension error in macro action object request")
        except rospy.ServiceException as e:
            rospy.logerr("Service call failed: %s" % e)

    def on_connected_vehicles(self, msg):
        self.connected_vehicles = msg.vehicles_id


def robot_list_from_str(robot_id_list: str) -> list():
    robot_ids = list()
    for id_str in robot_id_list.split(" "):
        try:
            r_id = int(id_str)
            robot_ids.append(r_id)
        except ValueError:
            pass
    return robot_ids


def get_generators_for_robot(robot_id) -> list():
    rospy.wait_for_service(SrvStr.gen_diag)
    generators = dict()
    try:
        resp = rospy.ServiceProxy(SrvStr.gen_diag, SelfTest)()
        for gen in resp.status:
            for agents in gen.values:
                if agents.key == "robots":
                    if robot_id in robot_list_from_str(agents.value):
                        generators[gen.name] = gen.message == "enabled"
                        break
    except rospy.ServiceException as e:
        rospy.logerr(f"Service call to {SrvStr.gen_diag} failed: {e}")

    return generators


def generator_switch(gen_id: str, state: str) -> bool:
    """ Enable/Disable a generator

    Args:
        gen_id (str): Enable/Disable the generator with the given ID
        state (str):  Either 'enable' or 'disable'
    """
    # Find the corresponding service first

    if state != "enable" and state != "disable":
        return False

    srv_str = "/ma_system/generators/manager/" + state
    rospy.wait_for_service(srv_str)
    try:
        req = SetStringRequest()
        req.data = gen_id
        resp = rospy.ServiceProxy(srv_str, SetString)(req)
        rospy.loginfo(f"Service call to {srv_str} returned: '{resp.message}'")
        return resp.success
    except rospy.ServiceException as e:
        rospy.logerr(f"Service call to {srv_str} failed: {e}")
        return False


def set_robot_availability(robot_name: str, availability: bool) -> bool:
    srv_str = "/" + robot_name + "/set_robot_availability"
    rospy.wait_for_service(srv_str)
    try:
        req = SetBoolRequest()
        # It's not intuitive, but False sets the AMR available ¯\_(ツ)_/¯
        req.data = not availability
        resp = rospy.ServiceProxy(srv_str, SetBool)(req)
        rospy.loginfo(f"Service call to {srv_str} returned: '{resp.message}'")
        return resp.success
    except rospy.ServiceException as e:
        rospy.logerr(f"Service call to {srv_str} failed: {e}")
        return False


def test_go_charge(sys_obs: SystemObserver) -> bool:

    start = rospy.Time.now()

    while rospy.Time.now() - start < Timeout.initialization:
        if sys_obs.connected_vehicles:
            break
    else:
        rospy.logerr("Timeout for connected_vehicles reached!")
        return False

    robot_id = sys_obs.connected_vehicles[0]
    robot_name = id_ops.decodeId(robot_id)

    sys_obs.listen_to_amr(robot_id)

    amr = sys_obs.amrs[robot_id]

    rate = rospy.Rate(1)  # hz

    sys_obs.generators_for_robot[robot_id] = get_generators_for_robot(robot_id)
    amr_gens = sys_obs.generators_for_robot[robot_id]

    parking_gen_list = [
        g for g in amr_gens.keys()
        if "park" in g.lower() or "charge" in g.lower()]

    if not parking_gen_list:
        # TODO:
        rospy.logerr("Parking generators list is empty!!!")
        rospy.logwarn(
            "[TODO] Will try to directly inject a charge mission instead, but for now just fail")
        return False

    # Try to enable all generator in the parking_gen_list
    for gen in parking_gen_list:
        if amr_gens[gen]:
            rospy.logwarn(f"Generator {gen} is already enabled")
            break
        rospy.loginfo(f"Enabling generator: {gen}")
        if not generator_switch(gen, "enable"):
            rospy.logerr(f"Failed to enable parking generator {gen}")
            return False
        sys_obs.enabled_gens.add(gen)

    start = rospy.Time.now()

    while rospy.Time.now() - start < Timeout.initialization:
        if amr.is_init():
            break
    else:
        rospy.logerr("Timeout for amr initialization reached!")
        return False

    if not amr.states.is_available:
        if not set_robot_availability(robot_name, True):
            rospy.logerr(
                f"Failed to make the robot {robot_name} available")
            return False
    else:
        rospy.logwarn(
            "Skip setting robot availability, robot is already available")

    start = rospy.Time.now()

    # monitor list of macro actions, one should appear before some timeout
    while rospy.Time.now() - start < Timeout.macro_action:
        if sys_obs.macro_action_agents:
            break
        rate.sleep()
    else:
        rospy.logerr("Timeout for macro_action reached!")
        return False

    # get the info
    for ma_id, agents in sys_obs.macro_action_agents.items():
        if robot_id in agents["robots"]:
            # mission started
            rospy.loginfo(
                f"Found macro action of ID '{ma_id}' concerning robot {robot_name}")
            break
    else:
        # end of all macro_actions_agents dictionary
        rospy.logerr(
            f"Unable to find robot {robot_name} in the macro action agents list")
        return False

    start = rospy.Time.now()

    # the assigned interactable should be icharger_ ...
    while rospy.Time.now() - start < Timeout.assigned:
        if "charge" in amr.states.assigned_interac:
            break
        rate.sleep()
    else:
        # timeout reached
        rospy.logerr("Timeout for assigned reached!")
        return False

    rospy.loginfo(f"Assigned interactable: {amr.states.assigned_interac}")

    sleep(5)  # let some time pass to make sure robot is navigating (in Nav)

    start = rospy.Time.now()

    # the mode SM should be in Proc before some timeout
    while rospy.Time.now() - start < Timeout.navigation:
        if amr.states.mode == "Proc":
            break
        rate.sleep()
    else:
        # timeout reached
        rospy.logerr("Timeout for navigation reached!")
        return False

    start = rospy.Time.now()

    # KEEP THE FOLLOWING CODE, IT'S GOING TO BE USED WHEN WE SHIFT TO KASIM
    # the charging SM should go to state RelayOn before some timeout
    # while rospy.Time.now() - start < Timeout.charging_state:
    #     if amr.states.charging == "RelayOn":
    #         break
    #     rate.sleep()
    # else:
    #     # timeout reached
    #     rospy.logerr("Timeout for charging_state reached!")
    #     return False

    GREEN = "\033[32;1m"
    RESET = "\033[0m"
    rospy.loginfo(f"{GREEN}SUCCESS!{RESET}")

    return True


def handle_add_bridge_resp(bridge_name: str, resp):
    if not resp.success:
        log_msg = f"Unable to add extra bridge '{bridge_name}' for simulation: '{resp.message}'"
        if "already exists" in resp.message:
            rospy.logwarn(log_msg)
        else:
            rospy.logerr(log_msg)
            sys.exit(-1)
    else:
        rospy.loginfo(
            f"Bridge '{bridge_name}' added successfully, return message: '{resp.message}'")


def add_extra_bridges(sys_obs: SystemObserver):

    # Wait for the public_server/add_bridge service to be available
    rospy.wait_for_service(SrvStr.pclt_add_bridge)

    # Create the service request containing the bridge description
    req = AddBridgeRequest()
    req.descriptor.type = BridgeDescriptor.PCLT
    req.descriptor.enabled = True
    req.descriptor.ros_timeout = 10.0
    req.descriptor.public_timeout = 15.0

    # Set bridge description for set_model_state
    req.descriptor.name = "sim/set_model_state"
    req.descriptor.ros_locator = SrvStr.set_model_state
    req.descriptor.msg_type = "gazebo_msgs::SetModelState"

    add_bridge_proxy = rospy.ServiceProxy(SrvStr.pclt_add_bridge, AddBridge)

    resp = add_bridge_proxy(req)
    handle_add_bridge_resp(req.descriptor.name, resp)

    # Set bridge description for get_model_state
    req.descriptor.name = "sim/get_model_state"
    req.descriptor.ros_locator = "simulator/get_model_state"
    req.descriptor.msg_type = "gazebo_msgs::GetModelState"

    resp = add_bridge_proxy(req)
    handle_add_bridge_resp(req.descriptor.name, resp)

    for robot_id, _ in sys_obs.amrs.items():
        robot_name = id_ops.decodeId(robot_id)
        short_r_name = id_ops.shortDecodeId(robot_id)
        # Set bridge description for set_initpose_from_sim_ground_truth
        req.descriptor.name = f"{short_r_name}/set_initpose_from_sim_ground_truth"
        req.descriptor.ros_locator = f"{robot_name}/set_initpose_from_sim_ground_truth"
        req.descriptor.public_locator = f"{robot_name}/set_initpose_from_sim_ground_truth"
        req.descriptor.msg_type = "std_srvs::Empty"

        resp = add_bridge_proxy(req)
        handle_add_bridge_resp(req.descriptor.name, resp)


def reset_sys_state(sys_obs: SystemObserver):

    already_changed_gen = []
    already_aborted_ma = []

    for robot_id, amr in sys_obs.amrs.items():
        robot_name = id_ops.decodeId(robot_id)

        rospy.loginfo(f"Enabled generators: {sys_obs.enabled_gens}")

        # Disable generators
        for gen_name, gen_init_as_enabled in sys_obs.generators_for_robot[robot_id].items():
            if gen_name in already_changed_gen:
                continue
            if gen_name not in sys_obs.enabled_gens:
                continue
            if not gen_init_as_enabled:
                # disable this generator
                if generator_switch(gen_name, "disable"):
                    already_changed_gen.append(gen_name)

        # Abort macro action and clear assignations
        rospy.wait_for_service(SrvStr.abort_macro_action)

        for ma_id, agents in sys_obs.macro_action_agents.items():
            if ma_id not in already_aborted_ma:
                if robot_id in agents["robots"]:
                    # abort this ma_id
                    res = SetUInt64Response()
                    try:
                        req = SetUInt64Request()
                        req.data = ma_id
                        res = rospy.ServiceProxy(
                            SrvStr.abort_macro_action, SetUInt64)(req)
                    except rospy.ServiceException as e:
                        res.success = False
                        res.message = str(e)

                    if res.success:
                        already_aborted_ma.append(ma_id)

        rospy.wait_for_service(SrvStr.reset_robot)

        res = SetStringResponse()
        try:
            req = SetStringRequest()
            req.data = robot_name
            res = rospy.ServiceProxy(SrvStr.reset_robot, SetString)(req)
        except rospy.ServiceException as e:
            res.success = False
            res.message = str(e)

        sleep(5)  # wait for an eventual cancelation to start

        # After cancelation the robot should go to Stop
        start = rospy.Time.now()
        rate = rospy.Rate(1)  # hz

        # the robot SM should be in Stop before some timeout
        while rospy.Time.now() - start < Timeout.cancelation:
            if amr.states.robot == "Stop":
                break
            rate.sleep()
        else:
            # timeout reached
            rospy.logerr("Timeout for cancelation reached!")

        # Set unavailable
        if not amr.init_states.is_available:
            if not set_robot_availability(robot_name, False):
                rospy.logerr(
                    f"Failed to make the robot {robot_name} unavailable")

        sleep(2)  # wait for the robot to stop moving

        # Reset robot pose in the simulator and in the localization
        rospy.loginfo(
            f"Pose to restore for robot {robot_name}: {(amr.init_pose.x, amr.init_pose.y, amr.init_pose.theta)}")

        amr.reset_pose()

        # Set mode state machine to original state???


if __name__ == '__main__':

    rospy.init_node('activate_parking_generator', anonymous=True)

    sys_obs = SystemObserver()

    success = test_go_charge(sys_obs)

    add_extra_bridges(sys_obs)

    sleep(2)  # sleep a little so the bridges can be established

    reset_sys_state(sys_obs)

    if success:
        sys.exit(0)

    sys.exit(-1)
