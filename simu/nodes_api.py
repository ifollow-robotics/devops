#!/usr/bin/python3
import os
import re
import docker
import requests
import yaml
from simu import slack_alert
from tools.print_format import print_log, style
import json


def get_node_api_from_rosnode_info(prefix, rosnode_info):
    """Get the complete ROS node API for a node based on its rosnode info output
    """

    # Regular expression pattern to match API 3 classes
    pub_pattern = re.compile(r'Publications:(.*?)\n\n', re.DOTALL)
    sub_pattern = re.compile(r'Subscriptions:(.*?)\n\n', re.DOTALL)
    srv_pattern = re.compile(r'Services:(.*?)\n\n', re.DOTALL)

    # Regular expression pattern to match robot_names and interactables
    r_pattern = re.compile(r'ilogistics_\d_\d{4}')
    i_pattern = re.compile(r'\w+_\d_\d{4}')

    # Replacement string
    r_wildcard = '$(arg robot_name)'
    i_wildcard = '$(arg iteractable)'

    # Find matches for each section
    pubs = pub_pattern.search(rosnode_info)
    subs = sub_pattern.search(rosnode_info)
    srvs = srv_pattern.search(rosnode_info)

    # Function to extract items from the matched rosnode_info
    def extract_items(rosnode_info):
        if rosnode_info:
            items = set()
            for line in rosnode_info.group(1).strip().split('\n'):
                if len(line.split()) >= 2:

                    item = line.split()[1]

                    if item.find('set_logger_level') != -1 or \
                            item.find('get_loggers') != -1 or \
                            item == '/rosout' or \
                            item == '/clock':
                        continue

                    if prefix:
                        item = item.replace(prefix, '')

                    item = r_pattern.sub(r_wildcard, item)
                    item = i_pattern.sub(i_wildcard, item)

                    items.add(item)
            return list(items)
        return []

    # Extract and store the items in lists
    api = dict()
    api['srvs'] = extract_items(srvs)
    api['subs'] = extract_items(subs)
    api['pubs'] = extract_items(pubs)

    return api


def get_nodes_api_in_container(sim_id, ros_master):
    """Get the complete ROS nodes API from a running container
    """

    # Initialize nodes_api dict and get nodes list
    nodes_api = dict()

    container_name = sim_id + '-' + ros_master + '-1'

    client = docker.from_env()
    container = client.containers.get(container_name)

    cmd_list = f"/bin/bash -c 'source /ros_entrypoint.sh && rosnode list'"

    byte_list = container.exec_run(cmd_list).output
    nodes_list = byte_list.decode('utf-8').split()

    # Robot nodes will be namespaced with the name of the robots
    # Find the robot name of the first robot node in the node list and then stop
    prefix = None
    for node in nodes_list:
        if node.startswith('/ilogistics_'):
            prefix = '/' + node.split('/')[1]
            break

    # Create a blacklist of nodes that are not important or pose a problem
    nodes_blacklist = ['/gazebo', '/viewer_psub', '/rosout']

    # Go thru each node in nodes list and get its API
    for node in nodes_list:
        # Ignore the other robots
        if prefix and node.startswith('/ilogistics_') and not node.startswith(prefix):
            continue

        # Ignore nodes in the blacklist
        if node in nodes_blacklist:
            continue

        # Get the node info (node API)
        cmd_info = f"/bin/bash -c 'source /ros_entrypoint.sh && rosnode info {node}'"

        byte_list = container.exec_run(cmd_info).output
        rosnode_info = byte_list.decode('utf-8')

        # Strip the robot name from the name of the node
        if prefix:
            node = node.replace(prefix, '')

        # Parse the node info text into a dictionary representing the API
        nodes_api[node] = get_node_api_from_rosnode_info(prefix, rosnode_info)

    return nodes_api


def get_nodes_api(sim_id, nodes_list):
    """Get the complete ROS nodes API for the running simulation
    """

    # Use nodes_list to get all ros master containers
    nodes_api = dict()

    # "target" represents each ROS Master context
    # aka robots, server, and monitoring
    for target in nodes_list.keys():
        nodes_api[target] = get_nodes_api_in_container(
            sim_id, nodes_list[target]['ros_master_container'])

    return nodes_api


def get_prev_nodes_api(sim_env, rootdir='./'):
    """Get the ROS nodes API for this simulation environment from the
       previous successful pipeline
    """

    print(f'{style.bYELLOW}Downloading nodes API from '
          f'previous successful pipeline...{style.RESET}')

    branch = os.environ['CI_COMMIT_REF_NAME']
    proj_id = os.environ['CI_PROJECT_ID']
    job_name = os.environ['CI_JOB_NAME']

    headers = {"JOB-TOKEN": os.environ['CI_JOB_TOKEN']}

    url = f"http://gitlab.com/api/v4/projects/{proj_id}/jobs/" \
          f"artifacts/{branch}/raw/nodes_api/pipeline_job_data.yaml?job={job_name}"

    pipeline_data = requests.get(url, headers=headers, stream=True)
    if pipeline_data.status_code == 200:
        # Show the pipeline and job IDs which generated the previous node API data
        content = yaml.safe_load(pipeline_data.content.decode("utf-8"))
        print(f'Previous successful pipeline data: {content}')

        # Save it to this pipeline artifacts
        with open(f'{rootdir}nodes_api/{sim_env}/prev_pipeline_job_data.yaml', 'w') as fp:
            yaml.dump(content, fp, default_flow_style=False)

    else:
        print(f"{style.bRED}Failed to get pipeline and job data "
              f"from downloaded artifacts{style.RESET}")

    # Get the previous node API data
    url = f"http://gitlab.com/api/v4/projects/{proj_id}/jobs/" \
          f"artifacts/{branch}/raw/nodes_api/{sim_env}/api.yaml?job={job_name}"

    ret = requests.get(url, headers=headers, stream=True)

    if ret.status_code == 200:
        return ret.status_code, ret.content
    else:
        return ret.status_code, None


def advertise_api_change(rootdir='./', send_slack=True) -> bool:
    """Log information about API change supposedly created at sim_tests job
    """

    envs_with_api_warn = set()

    for env in os.listdir(f'{rootdir}nodes_api/'):
        api_diff_file = f'{rootdir}nodes_api/{env}/ddiff.json'
        prev_pipeline_data_file = f'{rootdir}nodes_api/{env}/prev_pipeline_job_data.yaml'
        if os.path.exists(api_diff_file):

            # Try to get the previous pipeline data used for comparison
            pipeline_data = None
            if os.path.exists(prev_pipeline_data_file):
                with open(prev_pipeline_data_file, 'r') as f:
                    pipeline_data = yaml.safe_load(f)

            print_log(f'Nodes API has changed for environment {style.UNDERLINE}{env}', 'error')

            if pipeline_data:
                print(f"{style.bRED}Differences from pipeline "
                      f"{style.bBLUE}{pipeline_data['pipeline_id']}{style.bRED}, "
                      f"job {style.bBLUE}{pipeline_data['job_id']}{style.bRED}:")
            else:
                print(f"{style.bRED}Differences:")

            ddiff = ''
            with open(api_diff_file, 'r') as f:
                ddiff = json.load(f)
            print(json.dumps(ddiff, indent=4))
            print(style.RESET)
            envs_with_api_warn.add(env)

    if envs_with_api_warn and send_slack:
        slack_alert.notify_api_change(os.environ['CI_JOB_URL'] + '#L60',
                                      envs_with_api_warn,
                                      f'{rootdir}devops/simu/resources/api_change_slack_msg_template.json')

    return False if envs_with_api_warn else True
