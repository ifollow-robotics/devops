#!/bin/bash

YEL="\e[33;1m"
L_YEL="\e[93m"
RED="\e[31;1m"
GRE="\e[32;1m"
NGRE="\e[32m"
RST="\e[0m"
CHECK_MARK="\xE2\x9C\x94"
CROSS_MARK="\u2718"
UNDERL="\e[4m"

echo -e "${YEL}##### Getting submodules #####${RST}"

function set_ssh_key() {

    which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
    eval $(ssh-agent -s) > /dev/null
    echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
    mkdir -p ~/.ssh
    chmod 700 ~/.ssh
    echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
}

function get_extra_submod() {

    local root_dir=$(pwd)
    local folders_containing_arg=$(find -type d -name $1)
    local substr='.git/modules'

    for folder in $folders_containing_arg; do

        if echo $folder | grep -q "$substr"; then

            local mod_hier=$(echo $folder | sed -E 's/\.\/\.git\/modules\/|\/modules\// -> /g')
            echo -e "  ${L_YEL}[get_extra_submod] ${GRE}${CHECK_MARK}${L_YEL} found extra submod hierarchy:${GRE}$mod_hier${RST}"

            cd $folder
            # echo -e "  ${L_YEL}[get_extra_submod] ls $folder${RST}"
            # ls -alh
            local url=$(cat config | grep "url = .*$1.*" | sed  's/.*url = \(.*\)/\1/g')
            local sha=$(cat HEAD)
            echo -e "  ${L_YEL}[get_extra_submod] URL for submod $1: ${GRE}$url${RST}"
            cd $root_dir # get to root level
            cd ..       # get above root level
            git clone $url
            cd $1
            if [ -z "$2" ] || [[ $2 == at_commit ]]; then
                echo -e "  ${L_YEL}[get_extra_submod] SHA for submod $1: ${GRE}$sha${RST}"
                git checkout $sha
            fi
            git submodule sync --recursive
            git submodule update --depth 1 --init --recursive
            cd $root_dir # get to root level
        fi

    done
}

function get_extra_submodules() {

    local args=$(echo "$@" | tr -d '\n' | tr -d '\r')

    if [ -n "$args" ]; then

        if [ -n "$UPSTREAM_BRANCH" ]; then
            echo -e " ${L_YEL}Getting extra submodules at same commit as the one used by amr_v4:$UPSTREAM_BRANCH${RST}"
            git clone --depth 1 -j 18 --shallow-submodules --recursive -b $UPSTREAM_BRANCH git@gitlab.com:ifollow-robotics/v4/amr_v4.git
            # checkout_type='at_commit'
        else
            echo -e " ${L_YEL}Getting extra submodules at same commit as the one used by amr_v4 default branch${RST}"
            # echo -e " ${L_YEL}Getting submodules at their default branch but using amr_v4 to find their URL${RST}"
            git clone --depth 1 -j 18 --shallow-submodules --recursive git@gitlab.com:ifollow-robotics/v4/amr_v4.git
            # checkout_type='default'
        fi

        checkout_type='at_commit'
        cd amr_v4/

        for submod in $args; do
            echo -e " ${L_YEL}Calling get_extra_submod ${GRE}$submod ${RED}$checkout_type${RST}"
            get_extra_submod $submod $checkout_type
        done

        cd ../ && rm -rf amr_v4/

    fi
}

set_ssh_key
git submodule sync --recursive
git submodule update --depth 1 --init --recursive
get_extra_submodules $@
