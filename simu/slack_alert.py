import json
import requests

robotic_sw_team_webhook = 'https://hooks.slack.com/services/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'

def post_msg(msg, webhook):
    requests.post(webhook, data=json.dumps(msg), headers={
        'Content-type': 'application/json'})


def notify_api_change(job_url, envs, template_path):

    # load json
    msg = None
    with open(template_path, 'r') as f:
        msg = json.load(f)

    if not msg:
        return

    # set URL for button
    msg['attachments'][0]['blocks'][3]['accessory']['url'] = job_url
    msg['attachments'][0]['blocks'][2]['text']['text'] = \
        f'\n*Concerned environments:*\n`{envs}`'

    post_msg(
        msg, robotic_sw_team_webhook)
