import os
import sys
# Used to make current folder available to import
sys.path.append(os.path.join(os.path.dirname(__file__), '.'))
from print_format import style


def get_env_var(env_var_name, show=True):

    try:
        # save ENV_IMAGE_TAG value (possibly set by the CI to be equal $CI_COMMIT_REF_NAME)
        env_var_value = os.environ[env_var_name]
        if show:
            print(style.bGREEN +
                  f"{env_var_name}: {env_var_value}" + style.RESET)

    except KeyError as err:
        env_var_value = None
        if show:
            print(style.ORANGE + f"---\nEnvironment var {env_var_name} not set for this pipeline\n"
                  "It may be overwritten by .env file for each simulation environment instead" + style.RESET)

    return env_var_value
