#!/usr/bin/python3

SCREEN_SIZE = 90


class style():
    BLACK = '\033[30m'
    bRED = '\033[31;1m'
    bGREEN = '\033[32;1m'
    bYELLOW = '\033[33;1m'
    bBLUE = '\033[34;1m'
    MAGENTA = '\033[35m'
    CYAN = '\033[36m'
    WHITE = '\033[37m'
    UNDERLINE = '\033[4m'
    BOLD = '\033[1m'
    RESET = '\033[0m'
    ORANGE = '\033[38;5;202m'


def print_log(message, type='info'):
    """
    Usage: print_log("my warning message", "warning")
    """

    type_style = None
    if type == 'info':
        type_style = style.bBLUE
    elif type == 'warning':
        type_style = style.bYELLOW
    elif type == 'error':
        type_style = style.bRED
    elif type == 'success':
        type_style = style.bGREEN

    spaces_to_center = round((SCREEN_SIZE - len(message)) / 2.0)

    print(type_style + '#' * SCREEN_SIZE + style.RESET)
    print(type_style + ' ' * spaces_to_center + message + style.RESET)
    print(type_style + '#' * SCREEN_SIZE + style.RESET)
