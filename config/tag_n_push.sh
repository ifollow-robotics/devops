#!/bin/bash

function tag_n_push()
{
    if [ -z "$UPSTREAM_BRANCH" ] || [ "$UPSTREAM_BRANCH" == "master" ]; then

        echo -e "Image $SHORT_CI_REGIS_IMAGE:$CI_PIPELINE_ID tagged as \033[1;92m$SHORT_CI_REGIS_IMAGE:$CI_COMMIT_REF_NAME\033[0m"
        docker tag "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID" "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"

        echo -e "Pushing \033[1;92m$SHORT_CI_REGIS_IMAGE:$CI_COMMIT_REF_NAME\033[0m image to GitLab Container Registry..."
        docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"

        if [[ "$CI_COMMIT_REF_NAME" == "master" ]]; then

            echo -e "Image $SHORT_CI_REGIS_IMAGE:$CI_PIPELINE_ID tagged as \033[1;92m$SHORT_CI_REGIS_IMAGE:$1\033[0m"
            docker tag "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID" "$CI_REGISTRY_IMAGE:$1"

            echo -e "Pushing \033[1;92m$SHORT_CI_REGIS_IMAGE:$1\033[0m image to GitLab Container Registry..."
            docker push "$CI_REGISTRY_IMAGE:$1"
        fi

    else

        echo -e "Image $SHORT_CI_REGIS_IMAGE:$CI_PIPELINE_ID tagged as \033[1;92m$SHORT_CI_REGIS_IMAGE:$UPSTREAM_BRANCH\033[0m"
        docker tag "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID" "$CI_REGISTRY_IMAGE:$UPSTREAM_BRANCH"

        echo -e "Pushing \033[1;92m$SHORT_CI_REGIS_IMAGE:$UPSTREAM_BRANCH\033[0m image to GitLab Container Registry..."
        docker push "$CI_REGISTRY_IMAGE:$UPSTREAM_BRANCH"

    fi
}
