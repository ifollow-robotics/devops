#!/bin/bash

source ./devops/config/get_submodules_utils.sh

echo -e "\033[33;1m##### Getting submodules for simulated_robots #####\033[0;m"

which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
eval $(ssh-agent -s) > /dev/null
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

# remove doc folder
rm -rf ./doc

# remove all files in root repository except .gitmodules
git submodule sync --recursive
git submodule update --depth 1 --init --recursive

mkdir -p ./modules
cd modules

# List needed submodules from amr_v4.
# In ifollow_common: messages/*ALL* json_loader task_definition init_pkg.
common_list=("messages/*" "json_loader" "task_definition" "init_pkg")
# In ifollow_tools: ifollow_utils spline_tools iipp_recursive_approach state_machines percep_utils groups_utils
tools_list=("ifollow_utils" "spline_tools" "iipp_recursive_approach" "state_machines" "percep_utils" "groups_utils")
# In essentials: lift_board main_board joy_cmd low_lvl_manager.
essentials_list=("lift_board" "main_board" "joy_cmd" "low_lvl_manager")
# In mas: *ALL*.
mas_list=("mas/*")
# In nav_loc repos: nmp_modules nav_governor robot_localization.
nav_list=("nmp_modules" "nav_governor" "robot_localization")

if [ -n "$UPSTREAM_BRANCH" ]; then
    echo -e "\e[93m Getting repos at the same commit as the one used by amr_v4:$UPSTREAM_BRANCH\e[0m"

    # Get ifollow_common and mas commits from amr_v4 project with UPSTREAM_BRANCH
    git clone -b $UPSTREAM_BRANCH git@gitlab.com:ifollow-robotics/v4/amr_v4.git
    cd amr_v4/
    # Retrieve needed submodules.
    common_repos=$(extract_submodules_paths "${common_list[@]}")
    clone_repos_on_pointed_commit "$common_repos"

    tools_repos=$(extract_submodules_paths "${tools_list[@]}")
    clone_repos_on_pointed_commit "$tools_repos"

    essentials_repos=$(extract_submodules_paths "${essentials_list[@]}")
    clone_repos_on_pointed_commit "$essentials_repos"

    mas_repos=$(extract_submodules_paths "${mas_list[@]}")
    clone_repos_on_pointed_commit "$mas_repos"

    nav_repos=$(extract_submodules_paths "${nav_list[@]}")
    clone_repos_on_pointed_commit "$nav_repos"

    # Remove uneeded amr_v4 repo.
    cd ../ && rm -rf amr_v4/

    # Other repositories not in amr_v4 project.

    # Pipeline has been triggered by an upstream project.
    # Retrieve ifollow_rviz_plugins from mas_dashboard project to make sure the
    # project points to the same commit.
    # By default, use onboard_dev branch except if master branch is triggered.
    if [ "$UPSTREAM_BRANCH" = "master" ]; then
        echo -e "\e[93m Getting repos at the same commit as the one used by mas_dashboard:master\e[0m"
        git clone -b master git@gitlab.com:ifollow-robotics/mas/mas_dashboard.git
    else
        echo -e "\e[93m Getting repos at the same commit as the one used by mas_dashboard:onboard_dev\e[0m"
        git clone -b onboard_dev git@gitlab.com:ifollow-robotics/mas/mas_dashboard.git
    fi
    cd mas_dashboard/
    mas_dashboard_list=("ifollow_rviz_plugins")
    rviz_plugin_repo=$(extract_submodules_paths "${mas_dashboard_list[@]}")
    clone_repos_on_pointed_commit "$rviz_plugin_repo"

    # Remove uneeded mas_dashboard repo.
    cd ../ && rm -rf mas_dashboard/

    # Remaining repos.
    git clone --depth 1 -b onboard_dev --recursive git@gitlab.com:ifollow-robotics/common/ifollow_visualization.git
    git clone --depth 1 -b onboard_dev --recursive git@gitlab.com:ifollow-robotics/mas/stoplights_srv.git
    git clone --depth 1 -b onboard_dev --recursive git@gitlab.com:ifollow-robotics/mas/mas_viz.git
else
    echo -e "\e[93m No UPSTREAM branch triggered, getting dependencies on onboard_dev branch\e[0m"

    git clone -b onboard_dev git@gitlab.com:ifollow-robotics/v4/amr_v4.git
    cd amr_v4/
    # Retrieve needed submodules.
    common_repos=$(extract_submodules_paths "${common_list[@]}")
    clone_repos_on_main_branch "$common_repos"

    tools_repos=$(extract_submodules_paths "${tools_list[@]}")
    clone_repos_on_main_branch "$tools_repos"

    essentials_repos=$(extract_submodules_paths "${essentials_list[@]}")
    clone_repos_on_main_branch "$essentials_repos"

    mas_repos=$(extract_submodules_paths "${mas_list[@]}")
    clone_repos_on_main_branch "$mas_repos"

    nav_repos=$(extract_submodules_paths "${nav_list[@]}")
    clone_repos_on_main_branch "$nav_repos"

    # Remove uneeded amr_v4 repo.
    cd ../ && rm -rf amr_v4/

    # Other repositories not in amr_v4 project.
    git clone --depth 1 -b onboard_dev --recursive git@gitlab.com:ifollow-robotics/common/ifollow_rviz_plugins.git
    git clone --depth 1 -b onboard_dev --recursive git@gitlab.com:ifollow-robotics/common/ifollow_visualization.git
    git clone --depth 1 -b onboard_dev --recursive git@gitlab.com:ifollow-robotics/mas/stoplights_srv.git
    git clone --depth 1 -b onboard_dev --recursive git@gitlab.com:ifollow-robotics/mas/mas_viz.git
fi

cd ../ # cd modules
