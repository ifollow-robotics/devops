#!/bin/bash

YEL="\e[33;1m"
RED="\e[31;1m"
GRE="\e[32;1m"
NGRE="\e[32m"
RST="\e[0m"
CHECK_MARK="\xE2\x9C\x94"
CROSS_MARK="\u2718"
UNDERL="\e[4m"

echo -e "${YEL}##### Build and Run unit tests #####${RST}"

source ./devops/config/registry_login.sh
registry_login

DOCKER_NETWORK_ID=$(docker network create -d bridge utest_job_network_$CI_PIPELINE_ID)

CROSSBAR_HOSTNAME=crossbar-rtr

echo -e "${YEL}Run a container for unit testing${RST}"
REGISTRY_IMAGE_CONT_ID=$(docker run -d --rm --network=$DOCKER_NETWORK_ID \
                                            --name utest_job_sw_$CI_PIPELINE_ID \
                                            "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID" sleep 7200)

function build_tests() {

    docker exec -t -e HOME_DIR=$HOME_DIR \
        -e CROSSBAR_HOSTNAME=$CROSSBAR_HOSTNAME \
        $REGISTRY_IMAGE_CONT_ID bash -c 'source /ros_entrypoint.sh && \
                     roscore & \
                     cd ${HOME_DIR}/catkin_ws/ && \
                     CPUS=$(echo "$(nproc) / 4" | bc) && \
                     if [ "$CPUS" -eq 0 ]; then CPUS=1; fi && \
                     catkin build -j"$CPUS" --mem-limit=25% --force-color --no-status \
                        -DENABLE_COVERAGE=true \
                        -DCMAKE_CXX_FLAGS="-DPACKAGE_ROS_VERSION=1 \
                        -Wno-int-in-bool-context -Wno-unused-variable \
                        -Wno-unused-but-set-variable -Wno-ignored-attributes \
                        -Wno-format -Wno-subobject-linkage -Wno-return-type \
                        -Wno-register" --catkin-make-args run_tests \
                     -- '
    return $?
}

function check_tests() {
    docker exec -t -e HOME_DIR=$HOME_DIR \
        $REGISTRY_IMAGE_CONT_ID bash -c 'source /ros_entrypoint.sh && \
                     cd ${HOME_DIR}/catkin_ws/ && \
                     catkin_test_results'
    return $?
}

function glob_reports() {
    docker exec -t -e HOME_DIR=$HOME_DIR \
        $REGISTRY_IMAGE_CONT_ID bash -c 'mkdir -p ${HOME_DIR}/tests_reports && \
                     cd ${HOME_DIR}/catkin_ws/build && \
                     find . -type f -regex ".*rostest.*" -exec rm {} \; && \
                     for i in $(find . -type f -regex ".*test_results.*\.xml"); \
                     do \
                         cp "$i" ${HOME_DIR}/tests_reports/$(echo "${i:2}" | tr / _); \
                     done && \
                     gcovr --xml-pretty --exclude-unreachable-branches --print-summary\
                       --gcov-ignore-parse-errors=negative_hits.warn_once_per_file\
                       -o ${HOME_DIR}/tests_reports/coverage.xml --filter ${HOME_DIR}/catkin_ws/src'
    return 0
}

function deploy_crossbar() {

    export CROSSBAR_CONT_ID=$(docker run -d --rm --hostname=$CROSSBAR_HOSTNAME \
                                                 --network=$DOCKER_NETWORK_ID \
                                                 --name utest_job_crossbar_$CI_PIPELINE_ID \
                                                 crossbario/crossbar)
}

# get home dir
if [[ $1 == "" ]]; then
    HOME_DIR="/home/ifollow"
else
    HOME_DIR=$1
fi

# deploy wamp broker
deploy_crossbar

# compile and run tests
echo -e "${YEL}Compile and run the tests${RST}"
if build_tests; then
    echo -e "${NGRE}${CHECK_MARK} Building tests was successful!!!${RST}"
else
    echo -e "${RED}Building tests failed!!!${RST}"
    exit 1
fi

if [ -n $CROSSBAR_CONT_ID ]; then
    docker stop $CROSSBAR_CONT_ID
fi

# copy all tests reports to an arbitrary folder
echo -e "${YEL}Glob all tests reports${RST}"
glob_reports

echo -e "${YEL}${HOME_DIR}/tests_reports contents:${RST}"
docker exec -t $REGISTRY_IMAGE_CONT_ID ls -alh ${HOME_DIR}/tests_reports/

# copy all tests reports from container to builder space
echo -e "${YEL}Copy all reports to be uploaded as artifacts${RST}"
mkdir -p ./tests_reports/
docker cp $REGISTRY_IMAGE_CONT_ID:${HOME_DIR}/tests_reports/. ./tests_reports/

# check all results, return failure if any test didn't pass
echo -e "${YEL}Check unit tests results${RST}"
if check_tests; then
    echo -e "${NGRE}${CHECK_MARK} All tests were successful!${RST}"
    ut_report_result=0
else
    echo -e "${RED}Not all tests were successful! Check the tests' reports${RST}"
    ut_report_result=1
fi

# stop container
echo -e "${YEL}Stopping unit testing container...${RST}"
docker stop $REGISTRY_IMAGE_CONT_ID
echo -e "${NGRE}${CHECK_MARK} Unit testing container stopped${RST}"

# stop network
echo -e "${YEL}Stopping unit testing network...${RST}"
docker network rm $DOCKER_NETWORK_ID
echo -e "${NGRE}${CHECK_MARK} Unit testing network stopped${RST}"

if [[ $ut_report_result == 1 ]]; then
    exit 1
fi

exit 0
