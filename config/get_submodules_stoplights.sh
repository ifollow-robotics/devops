#!/bin/bash

source ./devops/config/get_submodules_utils.sh

echo -e "\033[33;1m##### Getting submodules for stoplights #####\033[0;m"

which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
eval $(ssh-agent -s) > /dev/null
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
git submodule sync --recursive
git submodule update --depth 1 --init --recursive

mkdir -p ./modules
cd modules

# List needed submodules from amr_v4.
# In ifollow_common: messages/*ALL*.
comm_list=("messages/*")
# In ifollow_tools: groups_utils ifollow_utils spline_tools iipp_recursive_approach.
tools_list=("groups_utils" "ifollow_utils" "spline_tools" "iipp_recursive_approach")
# In tuw_multi_robot: tuw_multi_robot route_finder.
mas_list=("tuw_multi_robot" "route_finder")
# In nmp_modules: nmp_modules.
nav_list=("nmp_modules")

if [ -n "$UPSTREAM_BRANCH" ]; then
    echo -e "\e[93m Getting repos at the same commit as the one used by amr_v4:$UPSTREAM_BRANCH\e[0m"

    # Get ifollow_common and mas commits from amr_v4 project with UPSTREAM_BRANCH
    git clone -b $UPSTREAM_BRANCH git@gitlab.com:ifollow-robotics/v4/amr_v4.git || true
    cd amr_v4/
    # Retrieve needed submodules in amr_v4.
    common_repos=$(extract_submodules_paths ${comm_list[@]})
    clone_repos_on_pointed_commit "$common_repos"

    tools_repos=$(extract_submodules_paths ${tools_list[@]})
    clone_repos_on_pointed_commit "$tools_repos"

    mas_repos=$(extract_submodules_paths ${mas_list[@]})
    clone_repos_on_pointed_commit "$mas_repos"

    nav_repos=$(extract_submodules_paths ${nav_list[@]})
    clone_repos_on_pointed_commit "$nav_repos"

    # Remove uneeded amr_v4 repo.
    cd ../ && rm -rf amr_v4/
else
    echo -e "\e[93m No UPSTREAM branch triggered, getting dependencies on onboard_dev branch\e[0m"

    git clone -b onboard_dev git@gitlab.com:ifollow-robotics/v4/amr_v4.git
    cd amr_v4/
    # Retrieve needed submodules.
    comm_repos=$(extract_submodules_paths ${comm_list[@]})
    clone_repos_on_main_branch "$comm_repos"

    tools_repos=$(extract_submodules_paths ${tools_list[@]})
    clone_repos_on_main_branch "$tools_repos"

    mas_repos=$(extract_submodules_paths ${mas_list[@]})
    clone_repos_on_main_branch "$mas_repos"

    nav_repos=$(extract_submodules_paths ${nav_list[@]})
    clone_repos_on_main_branch "$nav_repos"

    # Remove uneeded amr_v4 repo.
    cd ../ && rm -rf amr_v4/
fi

cd ../ # cd modules
