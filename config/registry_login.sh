#!/bin/bash

function registry_login()
{
    echo -e "\033[33;1m##### Registry login #####\033[0;m"
    if [[ -n "$CI_REGISTRY_USER" ]]; then
        echo "Logging to GitLab Container Registry with CI credentials..."
        docker login -u "$CI_REGISTRY_USER" -p "$CI_REGISTRY_PASSWORD" "$CI_REGISTRY"
        echo ""
    fi
}
