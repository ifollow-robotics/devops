#!/bin/bash

project_name=$1
project_branch=$2

if [ -z "$project_name" ]
then
    echo -e "\033[0;31m#### No project name argument was given, nothing to do here!\033[0m"
else
    echo -e "\033[33;1m##### Setup git access #####\033[0;m"

    which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
    eval $(ssh-agent -s) > /dev/null
    echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
    mkdir -p ~/.ssh
    chmod 700 ~/.ssh
    echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

    echo -e "\033[33;1m##### Clonning git project $project_name #####\033[0;m"

    if [ -z "$project_branch" ]
    then
        project_branch="master"
    fi
    git clone --depth 1 -b $project_branch --recursive git@gitlab.com:$project_name.git
fi
