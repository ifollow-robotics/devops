#!/usr/bin/env python3
"""
Tag docker image and git branch as stables and update changelog
"""
import os
import sys
# Make parent folder available to import
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from config.registry_login import registry_login
import re
import semver
import subprocess
import docker
import yaml
import shutil
from tools.print_format import style
from simu.nodes_api import advertise_api_change

# This script is responsible for tagging docker images and their corresponding
# git repositories with a new version. The version is determined based on the
# commit message type (breaking, feat, fix, etc.) and the latest tag on the
# onboard_dev branch.
# If the CI_DRY_RUN arg is set to true, the script runs without pushing any tag.
# In other cases, note that for other branches, the script does nothing
# EXCEPT IF the FORCED_VERSION arg is set to a valid semver version (valid
# format: X.Y.Z-suffix).
# In case of failure, please make sur the tags eventually pushed have been
# removed from the repositories before re-running the job!

SUCCESSFULLY_TAGGED_REPOS = []

# Utils to run git commands.
def git(*args):
    return subprocess.check_output(["git"] + list(args))

# Identify if CI_DRY_RUN arg has been set to true (note that this arg is
# responsible for running the whole job without pushing any tag).
def is_ci_dry_run():
    if 'CI_DRY_RUN' in os.environ and \
            os.environ['CI_DRY_RUN'].lower() == 'true':
        return True
    return False

# Extract version from FORCED_VERSION input; if it does not suit the semver
# format, return None.
def extract_forced_version():
    forced_version = os.environ['FORCED_VERSION']
    match = re.match(r"^(\d+\.\d+\.\d+)(-.+)?$", forced_version)
    if (match):
        print(f"{style.bYELLOW}Will tag FORCED_VERSION: {forced_version}.{style.RESET}")
        return forced_version
    else:
        print(f"{style.ORANGE}Invalid FORCED_VERSION: {forced_version}.{style.RESET}")
        return None

# bump_version is responsible for identifying latest tag on onboard_dev branch
# and increment it based on commit message type.
# Returns None if no tag is found or if the commit is already tagged.
def bump_version():
    """
    Bump version number based on commit message type
     - BREAKING:      bumps major version
     - feat:          bumps minor version
     - anything else: bumps patch version
    """
    try:
        # Fetch all tags on onboard_dev branch.
        git('fetch', '--unshallow')
        git('fetch', 'origin', 'onboard_dev')
        git('checkout', 'onboard_dev')
        git('pull')
        # Get all tags
        all_tags = git('tag', '--merged', 'onboard_dev', '--sort=creatordate')
        # Filter -dev tags
        if isinstance(all_tags, bytes):
            all_tags = all_tags.decode('utf-8')
        version_list = all_tags.strip().split('\n')
        dev_versions = [v for v in version_list if '-dev' in v]
        # Get latest dev tag.
        latest_dev_version = dev_versions[-1] if dev_versions else None
        if latest_dev_version is None:
            print("No dev tags found in onboard_dev branch of repository.")
            return None
        # Get only version digits
        latest_version_digits = re.search(r'[\d.]+', str(latest_dev_version)).group(0)
    except (subprocess.CalledProcessError, IndexError):
        print("Subprocess error while fetching tags.")
        return None
    else:

        commit_id = git("rev-parse", "HEAD").decode().split()[0]
        commit_id_from_tag = git(
            "rev-list", f"tags/{latest_dev_version}").decode().split()[0]

        if commit_id != commit_id_from_tag:
            commit_msg = git("log", "-1", "--pretty=%B").decode().strip()
            commit_type = commit_msg.split(':')[0].lower()

            if commit_type == 'breaking':
                res_version = semver.bump_major(latest_version_digits)
                return str(res_version) + "-dev"
            elif commit_type == 'feat':
                res_version = semver.bump_minor(latest_version_digits)
                return str(res_version) + "-dev"
            else:
                res_version = semver.bump_patch(latest_version_digits)
                return str(res_version) + "-dev"
        else:
            print(f"{style.ORANGE}Could not tag commit because it has already "
                  f"been tagged with: {latest_dev_version}!{style.RESET} "
                  f"Run pipeline with FORCED_VERSION arg to force a new tag.")
            return None

# Identify version to bump:
# - manually set version -> use FORCED_VERSION input, should fit X.X.X-suffix;
# - onboard_dev branch -> automatic bump based on commit message type;
# - otherwise, do nothing (version == None, except if dry-run).
def get_version_tag(branch, dry_run):
    version = None
    if 'FORCED_VERSION' in os.environ:
        version = extract_forced_version()
    elif branch == 'onboard_dev' or dry_run:
        version = bump_version()
    return version

# Tag images and their corresponding repos with the new version.
# Tagged images are the main one (amr_v4) + its downstream.
def tag_images_and_their_repos(client, version_str, branch, dry_run):
    # Retrieve ci variables.
    ci_pipeline_id = os.environ['CI_PIPELINE_ID']
    ci_registry_img = os.environ['CI_REGISTRY_IMAGE']

    # Get images to tag: first one is the current one (amr_v4)
    extra_im_to_tag = [ci_registry_img]

    # Read images to tag from yaml file.
    try:
        with open('devops/config/images_to_tag.yaml', 'r') as file:
            file_data = yaml.safe_load(file)
            if 'images' in file_data and isinstance(file_data['images'], list):
                extra_im_to_tag.extend(file_data['images'])
    except FileNotFoundError:
        print("File images_to_tag.yaml not found.")
        return False
    except yaml.YAMLError as e:
        print(f"Error while reading YAML: {e}")
        return False

    for image in extra_im_to_tag:
        # Tag image and get sha1 of latest commit.
        commit_sha = tag_image(client, image, ci_pipeline_id, version_str, branch, dry_run)
        # Tag repo with commit sha1 and version.
        success = tag_repo_from_image_name(image, commit_sha, version_str, dry_run)
        if not success:
            return False

    return True

# Tag an image and returns sha1 of latest commit.
def tag_image(client, image, ci_pipeline_id, version_str, branch, dry_run):
    print(f"Tagging image {style.bBLUE}{image}{style.RESET}")
    client.api.pull(image + ':' + ci_pipeline_id)
    # Containerize image to tag.
    image_ending = image.split("/")[-1]
    container = client.containers.run(
        f'{image}:{ci_pipeline_id}', detach=True, tty=True,
        name=f'tagging_{image_ending}_{ci_pipeline_id}')
    # Write version to tag in container.
    # `sh -c` is a hack to send echo command with exec_run
    # https://github.com/docker/docker-py/issues/1637
    container.exec_run(
        ['sh', '-c', f'echo {version_str} > version.txt'])
    # Commit container to new image and push (only if not a dry-run).
    if not dry_run:
        # Tag as stable latest onboard_dev image.
        # Could be removed if not needed.
        if branch == 'onboard_dev':
            container.commit(image + ':stable')
            for out in client.images.push(image + ':stable', stream=True, decode=True):
                print(out)
            print('Pushed image ' + image + ':stable')
        # Tag image with version.
        container.commit(image + f':{version_str}')
        for out in client.images.push(image + f':{version_str}', stream=True, decode=True):
            print(out)
        print('Pushed image ' + image + f':{version_str}')
    else:
        print('Dry-run: image ' + image + ' not pushed.')
    # Extract sha1 of latest commit in container.
    commit_sha = container.exec_run(
        ['cat', '/commit_sha.txt']).output.decode('utf-8').rstrip()
    # Stop and remove container.
    container.stop()
    container.remove()
    print('Rm container: ' + image)
    return commit_sha

# Tag a repo associated with an image.
def tag_repo_from_image_name(image, commit_sha, version_str, dry_run):
    print(f"Tagging repo from image {style.bBLUE}{image}{style.RESET}"
          f": {commit_sha} -> {version_str}")
    # Extract repo URL from image name.
    repo_url = re.sub(r'.+gitlab.com/', r'git@gitlab.com:', image)
    success = tag_repo_from_url(repo_url, commit_sha, version_str, dry_run)
    return success

# Tag a repo with a given version on a specific commit sha1.
def tag_repo_from_url(url, commit_sha, version_str, dry_run):
    print(f"Repo {style.bBLUE}{url}{style.RESET}")
    project_name = url.rstrip(".git").split("/")[-1]
     # Try to clone project metadata.
    attempt = 0
    success = False
    while attempt < 5 and not success:
        try:
            git('clone', '--bare', url, project_name)
            success = True
        except subprocess.CalledProcessError:
            print(f"Error while cloning repo: {url}. Try again..")
            attempt += 1
    if not success:
        print(f"Error while cloning: {url}. Maximal number of retries reached.")
        return False
    # Check if input sha1 exists.
    os.chdir(project_name)
    try:
        git('branch', '-r', '--contains', commit_sha)
    except subprocess.CalledProcessError:
        print(f"Error: commit {commit_sha} does not exist in repo {url}.")
        os.chdir('../')
        shutil.rmtree(project_name, ignore_errors=True)
        return False
    # Tag repo with version.
    try:
        git('tag', version_str, commit_sha)
        # Push tag (only if not a dry-run).
        if not dry_run:
            git('push', 'origin', version_str)
    except subprocess.CalledProcessError:
        print(f"Error while tagging {project_name} with {version_str}.")
        os.chdir('../')
        shutil.rmtree(project_name, ignore_errors=True)
        return False
    # Clean and return true.
    print(f"Successfully tagged {project_name} with {version_str}.")
    SUCCESSFULLY_TAGGED_REPOS.append(os.getcwd())
    os.chdir('../')
    # Do not remove tagged repos to enable cleaning in case of failure.
    # shutil.rmtree(project_name, ignore_errors=True)
    return True

# Tag repositories defined in dummy_environment.yaml (artifact from previous
# sim_test job) with a given version.
def tag_extra_repos_for_compatibility(version_str, dry_run):
    print(f"Tagging {style.bBLUE}extra repos with {version_str}{style.RESET}")
    # Check if dummy_environment.yaml exists.
    if os.path.exists('./generic_proj_revisions/dummy_environment.yaml'):
        doc = None
        with open('./generic_proj_revisions/dummy_environment.yaml', 'r') as file:
            doc = yaml.safe_load(file)
        # Tmp directory to clone repos to tag.
        os.mkdir('repos_tagged_for_compatibility')
        os.chdir('repos_tagged_for_compatibility')
        # Tag each repo with version.
        for proj, proj_data in doc.items():
            print(f"Tagging {style.bBLUE}{proj}{style.RESET} with {version_str}")
            success = tag_repo_from_url(proj_data['remote_url'], proj_data['sha'], version_str, dry_run)
            if not success:
                os.chdir('../')
                return False
        # Clean and return true.
        os.chdir('../')
        shutil.rmtree('repos_tagged_for_compatibility', ignore_errors=True)
        return True
    else:
        print(f"{style.ORANGE}Error: dummy_environment not found.{style.RESET}")
        return False

def set_tag(client, version, branch, dry_run):
    # If version has been defined, use it as the new version to tag repos.
    if version != None:
        # Tag docker images and corresponding commits in tagged repos.
        print(f"Tagging git project and docker image "
              f"on branch {style.bBLUE}{branch}{style.RESET}, "
              f"with new version {style.bBLUE}{version}{style.RESET}")
        # Tag images and their repos (typically amr_v4 + images listed in
        # images_to_tag.yaml).
        success = tag_images_and_their_repos(client, version, branch, dry_run)
        if not success:
            return False
        # Tag remaining repos: no built image to tag, only keep track of commit
        # sha-1 used on specific repos. Those repos are defined in the artifact
        # of sim_tests job.
        # Typically: generic env, devops, and docker.
        success = tag_extra_repos_for_compatibility(version, dry_run)
        if not success:
            return False
    else:
        print(f"{style.ORANGE}No version to tag.{style.RESET}")
    # Everything worked as expected.
    return True

def clean_tagged_repos(version):
    if version is None:
        return True
    max_retries = 5
    root_dir = os.getcwd()
    global_success = True
    try:
        for dir in SUCCESSFULLY_TAGGED_REPOS:
            project_name = dir.split("/")[-1]
            print(f"Cleaning tag {style.bBLUE}{version}{style.RESET} pushed in "
                  f"{style.bBLUE}{project_name}{style.RESET}.")
            os.chdir(dir)
            # Remove tag.
            attempt = 0
            success = False
            while attempt < max_retries and not success:
                try:
                    git('push', '--delete',  'origin', version)
                    success = True
                except subprocess.CalledProcessError:
                    print(f"Error while cloning repo: {url}. Try again..")
                    attempt += 1
            if not success:
                print(f"Error while deleting tag: {version} in {project_name}. "
                      f"Maximal number of retries reached.")
                global_success = False
            # Remove directory.
            os.chdir(root_dir)
            shutil.rmtree(dir, ignore_errors=True)
    except:
        global_success = False
    if not global_success:
        print(f"{style.bRED}Error while cleaning tags pushed in repos. "
              f"Please remove tags by hand before re-running the job!{style.RESET}")
    return global_success

def main():
    # Global var, responsible to keep track of tagged repos.
    global SUCCESSFULLY_TAGGED_REPOS

    # Retrieve branch name.
    branch = os.environ['CI_COMMIT_REF_NAME']

    # Advertise API change and post message in robotics team slack channel.
    send_to_slack_channel = branch == 'onboard_dev'
    advertise_api_change('./', send_to_slack_channel)

    # Docker client init.
    client = docker.from_env()
    registry_login(client)

    # Identify if the current job is a dry-run: in such case, the whole job runs
    # but nothing is pushed!
    dry_run = is_ci_dry_run()
    if dry_run:
        print(f"{style.bYELLOW}Running in dry-run mode.{style.RESET}")

    # Get version to tag.
    version = get_version_tag(branch, dry_run)

    # Tag images and repos with new version.
    success = False
    try:
        success = set_tag(client, version, branch, dry_run)
    except Exception as e:
        print(f"{style.ORANGE}Error while tagging: {e}{style.RESET}")

    # Clean if not successful.
    if not success:
        clean_tagged_repos(version)
        return 1
    else:
        print(f"{style.bGREEN}Tagging successful!{style.RESET}")
        print(f"List of tagged repos:")
        for dir in SUCCESSFULLY_TAGGED_REPOS:
            project_name = dir.split("/")[-1]
            print(f" - {project_name}")
            shutil.rmtree(dir, ignore_errors=True)

if __name__ == "__main__":
    sys.exit(main())
