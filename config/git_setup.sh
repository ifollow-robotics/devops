#!/bin/bash

CI_REGISTRY_N_CHAR=${#CI_REGISTRY}
SHORT_CI_REGIS_IMAGE=${CI_REGISTRY_IMAGE:$CI_REGISTRY_N_CHAR + 1}

# Add permission to clone private projects
which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
eval $(ssh-agent -s) > /dev/null
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

if [[ -d .git ]]
then
    # Attempt to install git-lfs (not present by default)
    apk update > /dev/nulll && apk add --no-cache git-lfs > /dev/nulll

    # Get possible submdules and files from LFS
    git submodule sync --recursive
    git submodule update --depth 1 --init --recursive

    git lfs install || true # https://community.atlassian.com/t5/Bitbucket-articles/Bitbucket-Pipelines-Pipeline-failures-due-to-a-pre-push-hook/ba-p/2071337
    git lfs pull

    # Fix git remote url do we can update the git project root
    # the docker image using ssh
    git remote remove origin
    git remote add origin git@gitlab.com:${SHORT_CI_REGIS_IMAGE}.git
fi

# Get dep function wich takes a git url and an alternative parameter for the branch/tag
function get_dep()
{
    if [ -z $2 ]; then
        BRANCH_EXISTS=$(git ls-remote $1 --heads origin $CI_COMMIT_REF_NAME | wc -l)
        if [[ $BRANCH_EXISTS -eq 1 ]]; then
            echo -e "\033[33;1m##### Clonning $1 at [$CI_COMMIT_REF_NAME] #####\033[0;m"
            git clone --depth 1 -b $CI_COMMIT_REF_NAME --recurse-submodules $1
        else
            echo -e "\033[33;1m##### Clonning $1 at default branch #####\033[0;m"
            git clone --depth 1 --recurse-submodules $1
        fi
    else
        BRANCH_EXISTS=$(git ls-remote $1 --heads origin $2 | wc -l)
        if [[ $BRANCH_EXISTS -eq 1 ]]; then
            echo -e "\033[33;1m##### Clonning $1 at [$2] #####\033[0;m"
            git clone --depth 1 -b $2 --recurse-submodules $1
        else
            echo -e "\033[33;1m##### Clonning $1 at default branch #####\033[0;m"
            git clone --depth 1 --recurse-submodules $1
        fi
    fi
}
