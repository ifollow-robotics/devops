#!/bin/bash

echo -e "\033[33;1m##### Getting submodules for AMR #####\033[0;m"

which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
eval $(ssh-agent -s) > /dev/null
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
# remove doc folder
rm -rf ./doc
# remove all files in root repository except .gitmodules
#find . ! -name '.gitmodules' -type f -exec rm -f {} +
# git submodule foreach git pull origin master # https://stackoverflow.com/questions/38227598/git-submodule-update-init-gives-error-fatal-needed-a-single-revision-unable-t/42417617
git submodule sync --recursive
git submodule update --depth 1 --init --recursive

# EXTRA STUFF FOR LFS
apk update > /dev/nulll && apk add --no-cache git-lfs > /dev/nulll # attempt to install git-lfs (not present by default)

git lfs install || true # https://community.atlassian.com/t5/Bitbucket-articles/Bitbucket-Pipelines-Pipeline-failures-due-to-a-pre-push-hook/ba-p/2071337

cd perception/camera_detection/
echo -e "\033[0;32mGoing to execute GIT LFS for camera_detection\033[0m"
git lfs pull
cd ../../

# NEVER DO THE FOLLOWING HERE IN THE .gitlab-ci.yml
# git lfs install
# git submodule foreach --recursive git lfs pull
