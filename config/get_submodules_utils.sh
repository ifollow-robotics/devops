#!/bin/bash

# All these functions are used to get submodules from a main project.
# They should be called from main project directory.

extract_submodules_paths() {
    # Input args: list of submodules to extract paths from.
    local repos=("$@")
    for repo in "${repos[@]}"; do
        awk -F' = ' -v repo="$repo" '
            $1 ~ /path/ && $2 ~ repo {
                print $2
            }
        ' .gitmodules
    done
}

get_url_from_path() {
    local path="$1"  # Input path.
    local url=$(awk -v p="$path" '
        /^\[submodule/ {module=""; next}
        $1 == "path" && $3 == p {module=1}
        module && $1 == "url" {print $3; exit}
    ' .gitmodules)

    # Return URL if found (null string if not found).
    echo "$url"
}

# Clone repos on the commit pointed by the main project.
# The cloned repos are the ones listed in the input list, they are cloned in
# same directoy as the main project.
clone_repos_on_pointed_commit() {
    local root=$(pwd)
    local repos="$1"  # List of repos paths to clone.
    for repo_path in $repos; do
        # Get URL associated with input path.
        local url=$(get_url_from_path "$repo_path")
        if [[ -n "$url" ]]; then
            echo "URL found for $repo_path: $url"
            # Get commit.
            local commit=$(git ls-tree -r HEAD | grep "$repo_path" | awk '{print $3}')
            echo "Cloning $url on commit $commit"
            cd ..
            git clone -n "$url"
            local repo=$(echo "$url" | awk -F'/' '{print $NF}' | sed 's/.git$//')
            cd "$repo"
            git checkout "$commit"
            git submodule sync --recursive && git submodule update --init --recursive
            local current_commit=$(git rev-parse HEAD)
            echo "Project $url cloned on $current_commit"
            cd "$root"
        else
            echo "WARNING: no corresponding URL found for: $repo_path"
        fi
    done
}

# Clone repos on the main branch of the main project.
# The cloned repos are the ones listed in the input list, they are cloned in
# same directoy as the main project.
clone_repos_on_main_branch() {
    local root=$(pwd)
    local repos="$1"  # List of repos paths to clone.
    for repo_path in $repos; do
        # Get URL associated with input path.
        local url=$(get_url_from_path "$repo_path")
        if [[ -n "$url" ]]; then
            echo "URL found for $repo_path: $url"
            echo "Cloning $url on main branch"
            cd ..
            git clone --depth 1 --recursive "$url"
            cd "$repo"
            local current_commit=$(git rev-parse HEAD)
            local branch_name=$(git rev-parse --abbrev-ref HEAD)
            echo "Project $url cloned on branch $branch_name: $current_commit"
            cd "$root"
        else
            echo "WARNING: no corresponding URL found for: $repo_path"
        fi
    done
}