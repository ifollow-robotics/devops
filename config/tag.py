#!/usr/bin/env python3
"""
Tag docker image and git branch as stables and update changelog
"""
import sys
import os
# Make parent folder available to import
sys.path.append(os.path.join(os.path.dirname(__file__), '..'))
from config.registry_login import registry_login
import re
import semver
import subprocess
import docker
import yaml

def tag_docker_image(client, version):
    """
    - Tag docker image as `stable` and `<x.y.z-stable>`
    """

    stable_version = str(version) # no need to add suffix '-stable' here

    ci_pipeline_id = os.environ['CI_PIPELINE_ID']
    ci_registry_img = os.environ['CI_REGISTRY_IMAGE']

    client.api.pull(ci_registry_img + ':' + ci_pipeline_id)

    ci_reg_img_ending=ci_registry_img.split("/")[-1]

    container = client.containers.run(
        f'{ci_registry_img}:{ci_pipeline_id}', detach=True, tty=True, name=f'tagging_{ci_reg_img_ending}_{ci_pipeline_id}')

    # `sh -c` is a hack to send echo command with exec_run
    # https://github.com/docker/docker-py/issues/1637
    container.exec_run(['sh', '-c', f'echo {stable_version} > version.txt'])

    container.commit(ci_registry_img + ':stable')
    container.commit(ci_registry_img + f':{stable_version}')

    for out in client.images.push(ci_registry_img + ':stable', stream=True, decode=True):
        print(out)

    for out in client.images.push(ci_registry_img + f':{stable_version}', stream=True, decode=True):
        print(out)

    container.stop()
    container.remove()


def git(*args):
    return subprocess.check_output(["git"] + list(args))


def tag_repo(version):
    url = os.environ["CI_REPOSITORY_URL"]

    # Transforms the repository URL to the SSH URL
    # Example input: https://gitlab-ci-token:xxx@gitlab.com/threedotslabs/ci-examples.git
    # Example output: git@gitlab.com:threedotslabs/ci-examples.git
    push_url = re.sub(r'.+@([^/]+)/', r'git@\1:', url)

    git("remote", "set-url", "--push", "origin", push_url)

    git("tag", version)
    git("push", "origin", version)


def bump_version():
    """
    Bump version number based on commit message type
     - BREAKING:      bumps major version
     - feat:          bumps minor version
     - anything else: bumps patch version
    """
    try:
        # fetch all tags
        git("fetch", "--unshallow")
        all_tags = git("tag", "--sort=creatordate").decode().split()
        # get most recent valid tag
        latest_tag = None
        for candidate_tag in reversed(all_tags):
            try:
                semver.Version.parse(candidate_tag)
                latest_tag = candidate_tag
                break
            except ValueError as e:
                print(f'Error parsing version, {e}')
        # quit early if no valid version was found
        if not latest_tag:
            return False
        # get only version digits
        latest_version = re.search(r'[\d.]+', str(latest_tag)).group(0)
    except (subprocess.CalledProcessError, IndexError):
        print("No tags found in the repository")
        return False
    else:

        commit_id = git("rev-parse", "HEAD").decode().split()[0]
        commit_id_from_tag = git(
            "rev-list", f"tags/{latest_tag}").decode().split()[0]

        if commit_id != commit_id_from_tag:
            commit_msg = git("log", "-1", "--pretty=%B").decode().strip()
            commit_type = commit_msg.split(':')[0].lower()

            if commit_type == 'breaking':
                return semver.bump_major(latest_version)
            elif commit_type == 'feat':
                return semver.bump_minor(latest_version)
            else:
                return semver.bump_patch(latest_version)
        else:
            print(f"Commit already tagged {latest_tag}")
            return False


def main():
    client = docker.from_env()
    registry_login(client)

    version = bump_version()

    if version:
        print(
            f"Tagging git project and docker image with new version {version}")
        tag_docker_image(client, version)
        tag_repo(version)


if __name__ == "__main__":
    sys.exit(main())
