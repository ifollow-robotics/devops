#!/bin/bash

source ./devops/config/get_submodules_utils.sh

echo -e "\033[33;1m##### Getting submodules for ros autobahn #####\033[0;m"

common_list=("messages/*" "init_pkg")
tools_list=("ifollow_utils" "percep_utils" "graph_utils" "ifollow_topic_tools" "groups_utils")

if [ -n "$UPSTREAM_BRANCH" ]; then
    echo -e "\e[93m Getting repos at the same commit as the one used by amr_v4:$UPSTREAM_BRANCH\e[0m"

    git clone -b $UPSTREAM_BRANCH git@gitlab.com:ifollow-robotics/v4/amr_v4.git
    cd amr_v4/
    # Retrieve needed submodules.
    common_repos=$(extract_submodules_paths "${common_list[@]}")
    clone_repos_on_pointed_commit "$common_repos"

    tools_repos=$(extract_submodules_paths "${tools_list[@]}")
    clone_repos_on_pointed_commit "$tools_repos"

    # Remove uneeded amr_v4 repo.
    cd ../ && rm -rf amr_v4/
else
    echo -e "\e[93m No UPSTREAM branch triggered, getting dependencies on onboard_dev branch\e[0m"

    git clone -b onboard_dev git@gitlab.com:ifollow-robotics/v4/amr_v4.git
    cd amr_v4/
    # Retrieve needed submodules.
    common_repos=$(extract_submodules_paths "${common_list[@]}")
    clone_repos_on_main_branch "$common_repos"

    tools_repos=$(extract_submodules_paths "${tools_list[@]}")
    clone_repos_on_main_branch "$tools_repos"

    # Remove uneeded amr_v4 repo.
    cd ../ && rm -rf amr_v4/
fi
