#!/bin/bash

echo -e "\033[33;1m##### Getting submodules #####\033[0;m"

which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
eval $(ssh-agent -s) > /dev/null
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
# remove doc folder
rm -rf ./doc
git submodule sync --recursive
git submodule update --depth 1 --init --recursive
