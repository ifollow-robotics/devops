#!/bin/bash

source ./devops/config/get_submodules_utils.sh

echo -e "\033[33;1m##### Getting submodules for mas_dashboard #####\033[0;m"

which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
eval $(ssh-agent -s) > /dev/null
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
# remove doc folder
rm -rf ./doc
# remove all files in root repository except .gitmodules
#find . ! -name '.gitmodules' -type f -exec rm -f {} +
# git submodule foreach git pull origin master # https://stackoverflow.com/questions/38227598/git-submodule-update-init-gives-error-fatal-needed-a-single-revision-unable-t/42417617
git submodule sync --recursive
git submodule update --depth 1 --init --recursive

# List needed submodules from amr_v4.
# In ifollow_common: *ALL*.
common_list=("ifollow_common/*")
# In nav_loc: nmp_modules nav_governor robot_localization.
nav_list=("nmp_modules" "nav_governor" "robot_localization")
# In essentials: lift_board main_board joy_cmd low_lvl_manager sick_safetyscanners.
essentials_list=("lift_board" "main_board" "joy_cmd" "low_lvl_manager" "sick_safetyscanners")
# In mas: *ALL*.
mas_list=("mas/*")

if [ -n "$UPSTREAM_BRANCH" ]; then
    # Clone amr_v4 following upstream branch to make sure this pipeline uses
    # same version of submodules.
    echo -e "\e[93m Getting repos at the same commit as the one used by amr_v4:$UPSTREAM_BRANCH\e[0m"

    git clone -b $UPSTREAM_BRANCH git@gitlab.com:ifollow-robotics/v4/amr_v4.git
    cd amr_v4/
    # Retrieve needed submodules.
    common_repos=$(extract_submodules_paths "${common_list[@]}")
    clone_repos_on_pointed_commit "$common_repos"

    nav_repos=$(extract_submodules_paths "${nav_list[@]}")
    clone_repos_on_pointed_commit "$nav_repos"

    essentials_repos=$(extract_submodules_paths "${essentials_list[@]}")
    clone_repos_on_pointed_commit "$essentials_repos"

    mas_repos=$(extract_submodules_paths "${mas_list[@]}")
    clone_repos_on_pointed_commit "$mas_repos"

    # Remove uneeded amr_v4 repo.
    cd ../ && rm -rf amr_v4/
else
    echo -e "\e[93m No UPSTREAM branch triggered, getting dependencies on onboard_dev branch\e[0m"

    git clone -b onboard_dev git@gitlab.com:ifollow-robotics/v4/amr_v4.git
    cd amr_v4/
    # Retrieve needed submodules.
    common_repos=$(extract_submodules_paths "${common_list[@]}")
    clone_repos_on_main_branch "$common_repos"

    nav_repos=$(extract_submodules_paths "${nav_list[@]}")
    clone_repos_on_main_branch "$nav_repos"

    essentials_repos=$(extract_submodules_paths "${essentials_list[@]}")
    clone_repos_on_main_branch "$essentials_repos"

    mas_repos=$(extract_submodules_paths "${mas_list[@]}")
    clone_repos_on_main_branch "$mas_repos"

    # Remove uneeded amr_v4 repo.
    cd ../ && rm -rf amr_v4/
fi
