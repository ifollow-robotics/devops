#!/bin/bash

apk add cppcheck
apk add python3
apk add py3-pip
python3 -m pip install -U cppcheck_codequality

# Select the devOpsBranch
./select_branch.sh

export MISRA_PATH=$(dirname $0)

alias python="python3"
ln -s /usr/bin/python3.8 /usr/bin/python

# Analyse the code
cppcheck --enable=all --addon=./devops/config/cppcheck/misra.json --inline-suppr --xml-version=2 --std=c++17 -q --platform=unix64 -itest --output-file=cppcheck_out.xml ./

# Convert from xml to json file for gitlab
cppcheck-codequality --input-file=cppcheck_out.xml --output-file=cppcheck.json

for xmlfile in *.json
do
    if [ -n "$(head ${xmlfile} | egrep '\"severity\": \"critical\"|\"severity\": \"major\"|\"severity\": \"error\"')" ] ; then
        echo -e "Static code analysis Can't have Minor | Major | Warning | Error"
        exit 1
    fi
done
echo -e "Completed successfully"

# remove the xml file
rm cppcheck_out.xml
