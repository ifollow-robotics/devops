#!/usr/bin/env python3
"""
    Define function used to `docker login`
"""
import sys
sys.path.append('..')   # Used to make parent folder available to import

from docker.errors import APIError, TLSParameterError
import os
from tools.print_format import print_log, style


def docker_login(client):

    user = os.environ['REGISTRY_USER']
    pswd = os.environ['REGISTRY_PWD']
    gitlab_registry = os.environ['CI_REGISTRY']

    client.login(username=user, password=pswd, registry=gitlab_registry)


def registry_login(client):
    print_log("Registry login")

    try:
        print("Logging to GitLab Container Registry with CI credentials...")
        docker_login(client)
        print(style.bGREEN + "\t\t\t\tLogged in Gitlab registry" + style.RESET)
    except (APIError, TLSParameterError) as err:
        print(style.bRED + "Authentication error: {}".format(err) + style.RESET)
    except (KeyError) as err:
        print(style.bRED + "Environment variables not set" + style.RESET)
