#!/bin/bash

# select generic project branch
# if FORCED_VERSION_FROM_AMR_PIPELINE is set, override the version behavior.
# if SW_IMAGE_TAG is a version, use it to get the compatible generic revision.
# else, check of an hypothetical UPSTREAM_BRANCH and get generic at that
# revision or at the default branch.

if [[ -n "$FORCED_VERSION_FROM_AMR_PIPELINE" ]]; then
    echo "FORCED_VERSION_FROM_AMR_PIPELINE is set, overriding the version behavior"
    echo "FORCED_VERSION_FROM_AMR_PIPELINE: $FORCED_VERSION_FROM_AMR_PIPELINE"
    get_dep git@gitlab.com:ifollow-robotics/common/envs/generic.git $FORCED_VERSION_FROM_AMR_PIPELINE
elif [[ "$SW_IMAGE_TAG" =~ [0-9]+\.[0-9]+\.[0-9]+ ]]; then
    echo "SW_IMAGE_TAG is a version, getting the compatible generic revision"
    echo "SW_IMAGE_TAG: $SW_IMAGE_TAG"
    get_dep git@gitlab.com:ifollow-robotics/common/envs/generic.git $SW_IMAGE_TAG
else
    if [ -z $UPSTREAM_BRANCH ]; then
        echo "UPSTREAM_BRANCH is not set, getting the generic project at the default branch"
        get_dep git@gitlab.com:ifollow-robotics/common/envs/generic.git
    else
        echo "UPSTREAM_BRANCH is set, getting the generic project at the branch $UPSTREAM_BRANCH"
        get_dep git@gitlab.com:ifollow-robotics/common/envs/generic.git $UPSTREAM_BRANCH
    fi
fi

# Save the generic project's revision we used
cd generic
git rev-parse HEAD > .generic_revision_sha.txt
SHA1=$(git rev-parse HEAD)
echo "Clone generic project on $SHA1"
cd ..
