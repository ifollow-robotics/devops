#!/bin/bash

source ./devops/config/get_submodules_utils.sh

echo -e "\033[33;1m##### Getting submodules for ifollow monitor #####\033[0;m"

which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
eval $(ssh-agent -s) > /dev/null
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
rm -rf ./docs
git submodule sync --recursive
git submodule update --depth 1 --init --recursive

# List needed submodules from amr_v4.
# In ifollow_common: messages/*ALL* ifollow_tools/*ALL*.
common_list=("messages/*" "ifollow_tools/*")
# In mas: ros_autobahn.
mas_list=("ros_autobahn")
# In essentials: sick_safetyscanners.
essentials_list=("sick_safetyscanners")

if [ -n "$UPSTREAM_BRANCH" ]; then
    echo -e "\e[93m Getting projects at same commit as the one used by amr_v4:$UPSTREAM_BRANCH\e[0m"

    # Get required commits from amr_v4 project.
    git clone -b $UPSTREAM_BRANCH git@gitlab.com:ifollow-robotics/v4/amr_v4.git
    cd amr_v4/
    # Retrieve needed submodules.
    common_repos=$(extract_submodules_paths "${common_list[@]}")
    clone_repos_on_pointed_commit "$common_repos"

    mas_repos=$(extract_submodules_paths "${mas_list[@]}")
    clone_repos_on_pointed_commit "$mas_repos"

    essentials_repos=$(extract_submodules_paths "${essentials_list[@]}")
    clone_repos_on_pointed_commit "$essentials_repos"

    # Remove uneeded amr_v4 repo.
    cd ../ && rm -rf amr_v4/
else
    echo -e "\e[93m No UPSTREAM branch triggered, getting dependencies on onboard_dev branch\e[0m"

    git clone -b onboard_dev git@gitlab.com:ifollow-robotics/v4/amr_v4.git
    cd amr_v4/
    # Retrieve needed submodules.
    common_repos=$(extract_submodules_paths "${common_list[@]}")
    clone_repos_on_main_branch "$common_repos"

    mas_repos=$(extract_submodules_paths "${mas_list[@]}")
    clone_repos_on_main_branch "$mas_repos"

    essentials_repos=$(extract_submodules_paths "${essentials_list[@]}")
    clone_repos_on_main_branch "$essentials_repos"

    # Remove uneeded amr_v4 repo.
    cd ../ && rm -rf amr_v4/
fi

# Clean.
cd ros_autobahn/
find . -maxdepth 1 ! -name "public_launch" -exec rm -rf {} \; || true
cd ../