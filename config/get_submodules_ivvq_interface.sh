#!/bin/bash

echo -e "\033[33;1m##### Getting submodules for IVVQ INTERFACE TESTING TOOLS #####\033[0;m"

which ssh-agent > /dev/null || ( apk update && apk add openssh-client git ) > /dev/null
eval $(ssh-agent -s) > /dev/null
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config
rm -rf ./docs
echo "START SUBMODULE COMMAND"
git submodule sync --recursive
git submodule update --depth 1 --init --recursive
echo "END SUBMODULE COMMAND"
echo "GOING TO MAIN"

function get_submodule()
{
    local root_dir=$(pwd)
    local folders_containing_arg=$(find -type d -name $1)

    for folder in $folders_containing_arg
    do
        # git module full path
        if [[ $folder == *.git/modules* ]]; then
            cd $folder
            echo "[get_submodule] ls $folder"
            ls -alh
            local url=$(cat config | grep "url = " | sed  's/.*url = \(.*\)/\1/g')
            local sha=$(cat HEAD)
            echo "[get_submodule] URL for $1: $url"
            echo "[get_submodule] SHA for $1: $sha"
            cd root_dir # get to root level
            cd ..       # get above root level
            git clone --recursive --depth 1 $url
            cd $1
            if [ -z "$2" ] || [[ $2 == at_commit ]]; then
                git checkout $sha
            fi
            git submodule sync --recursive && git submodule update --init --recursive
            cd root_dir # get to root level
        fi

    done
}


if [ -n "$UPSTREAM_BRANCH" ]; then
    echo -e "\e[93m Getting submodules at commit $UPSTREAM_BRANCH\e[0m"

    git clone --depth 1 -b $UPSTREAM_BRANCH git@gitlab.com:ifollow-robotics/ivvq/interfaces-test-tool.git
    cd interfaces-test-tool/

    for submod in "$@"
    do
        get_submodule $submod 
    done

    cd ../

else

    echo -e "\e[93m Getting submodules at same their else branch\e[0m"
    echo -e "\e[93m Getting submodules at commit $UPSTREAM_BRANCH\e[0m"

    git clone --depth 1 -b $UPSTREAM_BRANCH git@gitlab.com:ifollow-robotics/ivvq/interfaces-test-tool.git
    cd interfaces-test-tool/

    for submod in "$@"
    do
        get_submodule $submod 
    done

    cd ../
fi

