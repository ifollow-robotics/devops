#!/bin/bash

echo -e "\033[33;1m##### Tagging #####\033[0;m"

source ./devops/config/registry_login.sh
registry_login
docker pull "$CI_APPLICATION_REPOSITORY:$CI_PIPELINE_ID"

if [ -z "$UPSTREAM_BRANCH" ] || [ "$UPSTREAM_BRANCH" == "master" ]
then

    echo "Image $CI_APPLICATION_REPOSITORY:$CI_PIPELINE_ID tagged as $CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    docker tag "$CI_APPLICATION_REPOSITORY:$CI_PIPELINE_ID" "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"
    echo "Pushing $CI_COMMIT_REF_NAME image to GitLab Container Registry..."
    docker push "$CI_REGISTRY_IMAGE:$CI_COMMIT_REF_NAME"

    if [[ "$CI_COMMIT_REF_NAME" == "master" ]]; then
        echo "Image $CI_APPLICATION_REPOSITORY:$CI_PIPELINE_ID tagged as $CI_REGISTRY_IMAGE:stable"
        docker tag "$CI_APPLICATION_REPOSITORY:$CI_PIPELINE_ID" "$CI_REGISTRY_IMAGE:stable"
        echo "Pushing stable image to GitLab Container Registry..."
        docker push "$CI_REGISTRY_IMAGE:stable"
    fi
else
    echo "Image $CI_APPLICATION_REPOSITORY:$CI_PIPELINE_ID tagged as $CI_REGISTRY_IMAGE:$UPSTREAM_BRANCH-CI-AUTOGEN"
    docker tag "$CI_APPLICATION_REPOSITORY:$CI_PIPELINE_ID" "$CI_REGISTRY_IMAGE:$UPSTREAM_BRANCH-CI-AUTOGEN"
    echo "Pushing $UPSTREAM_BRANCH-CI-AUTOGEN image to GitLab Container Registry..."
    docker push "$CI_REGISTRY_IMAGE:$UPSTREAM_BRANCH-CI-AUTOGEN"
fi

return 0

