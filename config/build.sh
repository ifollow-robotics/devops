#!/bin/bash

echo -e "\033[33;1m##### Building docker image #####\033[0;m"

source ./devops/config/registry_login.sh
source ./devops/config/tag_n_push.sh
registry_login

function add_commit_sha_file()
{
    REGISTRY_IMAGE_CONT_ID=$(docker run -d --rm \
                                           --name build_job_$CI_PIPELINE_ID \
                                           $1 sleep infinity)

    docker exec -t -u root -e CI_COMMIT_SHA=$CI_COMMIT_SHA \
        $REGISTRY_IMAGE_CONT_ID bash -c 'echo $CI_COMMIT_SHA > /commit_sha.txt'

    docker commit $REGISTRY_IMAGE_CONT_ID $1

    docker stop $REGISTRY_IMAGE_CONT_ID
}

# remove registry.gitlab.com prefix from 2 CI variables of interest (just for clearer logs)
CI_REGISTRY_N_CHAR=${#CI_REGISTRY}
CI_APPLICATION_REPOSITORY=${CI_REGISTRY_IMAGE}/${CI_COMMIT_REF_SLUG}
SHORT_CI_APP_REPO=${CI_APPLICATION_REPOSITORY:$CI_REGISTRY_N_CHAR + 1}
SHORT_CI_REGIS_IMAGE=${CI_REGISTRY_IMAGE:$CI_REGISTRY_N_CHAR + 1}

# Find out the quota value that uses 1/4 of the total CPUs of this machine
quota=$(echo "$(nproc) * 100000 / 4" | bc)
mem_lim=$(echo "$(grep MemTotal /proc/meminfo | awk '{print $2}') / 4000 " | bc)

if [ -f ros-base.Dockerfile ] && [ "$ROS_BASE_UPDATE" == "true" ]; then
    # ROS_BASE_UPDATE variable is set on ros project scheduled pipelines
    if docker build --build-arg PKGS_TO_BUILD=$PKGS_TO_BUILD \
            --secret id=priv_key,env=SSH_PRIVATE_KEY \
            --pull --no-cache --cpu-quota="$quota" -m="$mem_lim"m -f ros-base.Dockerfile \
            -t "$CI_APPLICATION_REPOSITORY:ros-base" .; then
        echo -e "Pushing \033[1;92m$SHORT_CI_APP_REPO:ros-base\033[0m to GitLab Container Registry..."
        if docker push "$CI_APPLICATION_REPOSITORY:ros-base"; then
            echo "Success image $SHORT_CI_APP_REPO:ros-base pushed"
        else
            echo "Error while PUSHING $SHORT_CI_APP_REPO:ros-base, continuing to build"
        fi
    else
        echo "Error while BUILDING $SHORT_CI_APP_REPO:ros-base, continuing to build"
    fi
    echo ""
fi

if [[ -f Dockerfile ]]; then

    echo -e "\033[33;1m##### Create USER and GROUP #####\033[0;m"
    C_USER=ifollow
    C_UID=1000
    C_GID=1000

    # Alpine
    addgroup -g $C_GID -S $C_USER
    adduser -D -H -S $C_UID -u $C_UID -G $C_USER

    if [ -n "$UPSTREAM_PIPELINE_ID" ]; then

        echo -e "\e[93m  Going to use image amr_v4:$UPSTREAM_PIPELINE_ID associated with upstream pipeline\e[0m"

        # There may be some case where amr_v4 images are used directly in the Dockerfile
        # replace the tags with the UPSTREAM_PIPELINE_ID
        sed -i 's/amr_v4:[^[:space:]]*/amr_v4:'"$UPSTREAM_PIPELINE_ID"'/g' Dockerfile

        echo -e "\e[93m  Going to use image ifollow_monitor:$UPSTREAM_PIPELINE_ID associated with upstream pipeline\e[0m"
        sed -i 's/ifollow_monitor:[^[:space:]]*/ifollow_monitor:'"$UPSTREAM_PIPELINE_ID"'/g' Dockerfile

        echo "Building Dockerfile-based application..."
        docker build --build-arg PKGS_TO_BUILD=$PKGS_TO_BUILD --pull --no-cache \
            --secret id=priv_key,env=SSH_PRIVATE_KEY \
            --cpu-quota="$quota" -m="$mem_lim"m -t "$CI_REGISTRY_IMAGE:$UPSTREAM_PIPELINE_ID" .

        add_commit_sha_file "$CI_REGISTRY_IMAGE:$UPSTREAM_PIPELINE_ID"

        echo -e "Pushing \033[1;92m$SHORT_CI_REGIS_IMAGE:$UPSTREAM_PIPELINE_ID\033[0m image to GitLab Container Registry..."
        docker push "$CI_REGISTRY_IMAGE:$UPSTREAM_PIPELINE_ID"

        echo -e "Image $SHORT_CI_REGIS_IMAGE:$UPSTREAM_PIPELINE_ID tagged as \033[1;92m$SHORT_CI_REGIS_IMAGE:$CI_PIPELINE_ID\033[0m"
        docker tag "$CI_REGISTRY_IMAGE:$UPSTREAM_PIPELINE_ID" "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID"
    else
        echo "Building Dockerfile-based application..."
        docker build --build-arg PKGS_TO_BUILD=$PKGS_TO_BUILD --pull --no-cache \
            --secret id=priv_key,env=SSH_PRIVATE_KEY \
            --cpu-quota="$quota" -m="$mem_lim"m -t "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID" .

        add_commit_sha_file "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID"
    fi

    echo -e "Pushing \033[1;92m$SHORT_CI_REGIS_IMAGE:$CI_PIPELINE_ID\033[0m image to GitLab Container Registry..."
    docker push "$CI_REGISTRY_IMAGE:$CI_PIPELINE_ID"

    tag_n_push nightly

else
    echo -e "\033[31;1mFail to build docker image\033[0;m"
fi

# TODO: remove image after pushing
