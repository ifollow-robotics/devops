#!/bin/bash

echo -e "\033[33;1m##### Setup Docker #####\033[0;m"

if ! docker info &>/dev/null; then
    if [ -z "$DOCKER_HOST" -a "$KUBERNETES_PORT" ]; then
        export DOCKER_HOST='tcp://localhost:2375'
    fi
fi
