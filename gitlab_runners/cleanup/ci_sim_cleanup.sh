#!/bin/bash

ANY_CI_SIM_ID="[1-9][0-9]{4}"
SIMULATION_ID=$ANY_CI_SIM_ID

function sim_cleanup()
{
    # Clear all containers, networks and volumes used by CI simulations
    # It uses the variable SIMULATION_ID (which can be a regex or a specific ID)

    docker stop $(docker ps -a | grep -E "[[:space:]]${SIMULATION_ID}-.*-[1-9]+$" | awk '{print $1}') > /dev/null 2>&1 || true
    docker rm $(docker ps -a | grep -E "[[:space:]]${SIMULATION_ID}-.*-[1-9]+$" | awk '{print $1}') > /dev/null 2>&1 || true
    docker network rm $(docker network list | grep -E "[[:space:]]${SIMULATION_ID}_[_[:alnum:]-]+[[:space:]]" | awk '{print $1}') > /dev/null 2>&1 || true
    docker volume rm $(docker volume ls | grep -E "[[:space:]]${SIMULATION_ID}_[_[:alnum:]-]+$" | awk '{print $2}') > /dev/null 2>&1 || true
}

sim_cleanup
