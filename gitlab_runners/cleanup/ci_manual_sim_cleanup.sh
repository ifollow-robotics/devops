#!/bin/bash

SEVEN_HOURS_AGO=$(echo "$(date +%s) - 25200"  | bc)

containers="-gzserver- -simulated-robot- -multi-agent-planner-
            -stoplights- -interactable- -picking-db- -server-monitor-
            -assign-robot- -websocket- -ros-autobahn- -logisticsio-
            -mycelium- -init-server- -mr-area-mngr- -api-rest-
            -crossbar-rtr- -mongodb- -mongodb-admin- -env-container- -postgresdb-"

function sim_cleanup()
{

    docker ps -a --format "{{.ID}} {{.CreatedAt}} {{.Names}}" | grep -E "[[:space:]].*$1[1-9]+$" | while read line

    do
        # line looks like:
        # 123456789abcdef 2017-01-01 00:00:00 +02:00 CEST 12345-container-name
        set $line
        id=$1
        # date doesn't like the CEST part so we skip it
        date=$(date -d "$(echo ${@:2:3})" +%s)
        if [ $date -le $SEVEN_HOURS_AGO ]; then
            docker stop $id
            docker rm $id
        fi
    done
}

for cont_name in $containers; do
    sim_cleanup $cont_name
done
