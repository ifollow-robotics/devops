#!/bin/bash

TWENTY_MIN_AGO=$(echo "$(date +%s) - 1200"  | bc)
TWO_HOURS_AGO=$(echo "$(date +%s) - 7200"  | bc)

function sim_cleanup()
{

    docker ps -a --format "{{.ID}} {{.CreatedAt}} {{.Names}}"  | grep -E "[[:space:]][1-9][0-9]{4}-" | while read line

    do
        # line looks like:
        # 123456789abcdef 2017-01-01 00:00:00 +02:00 CEST 12345-container-name
        set $line # sets $1 to id, $2 to date, $3 to hour
        id=$1
        # date doesn't like the CEST part so we skip it
        the_date=$(date -d "$(echo ${@:2:3})" +%s)
        if [ $the_date -le $TWENTY_MIN_AGO ]; then
            docker stop $id
            docker rm $id
        fi
    done
}

function job_containers_cleanup()
{
    docker ps -a --format "{{.ID}} {{.CreatedAt}} {{.Names}}"  | grep -E "[[:space:]]((tagging_)|(utest_job_)).*" | while read line

    do
        set $line # sets $1 to id, $2 to date, $3 to hour
        id=$1
        # date doesn't like the CEST part so we skip it
        the_date=$(date -d "$(echo ${@:2:3})" +%s)
        if [ $the_date -le $TWO_HOURS_AGO ]; then
            docker stop $id
            docker rm $id
        fi
    done
}

sim_cleanup
job_containers_cleanup
