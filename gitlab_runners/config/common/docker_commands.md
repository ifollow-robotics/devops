# Stop runner

```bash
docker stop gitlab-runner && docker rm gitlab-runner
```

# Start runner

```bash
docker run -d --name gitlab-runner --restart always -v /srv/gitlab-runner/config:/etc/gitlab-runner -v /var/run/docker.sock:/var/run/docker.sock gitlab/gitlab-runner:latest
```

Make sure the runner configuration file (`config.toml`) is correctly set here `/srv/gitlab-runner/config` based on `gitlab_runners/config/<runner_name>/config.toml`

