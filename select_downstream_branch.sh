#!/bin/bash

which ssh-agent > /dev/null || ( apk update && apk add --no-cache openssh-client git ) > /dev/null
eval $(ssh-agent -s) > /dev/null
echo "$SSH_PRIVATE_KEY" | tr -d '\r' | ssh-add - > /dev/null 2>&1
mkdir -p ~/.ssh
chmod 700 ~/.ssh
echo -e "Host *\n\tStrictHostKeyChecking no\n\n" > ~/.ssh/config

DOWNSTREAM_BRANCH_EXISTS=$(git ls-remote git@gitlab.com:$1.git --heads origin $CI_COMMIT_REF_NAME | wc -l)

if [[ $DOWNSTREAM_BRANCH_EXISTS -eq 1 ]]
then
    echo "$2=$CI_COMMIT_REF_NAME" >> deploy.env
    echo "Triggering downstream pipeline for $1 on branch [$CI_COMMIT_REF_NAME]"
else
    echo "$2=master" >> deploy.env
    echo "Triggering downstream pipeline for $1 on branch [master]"
fi



