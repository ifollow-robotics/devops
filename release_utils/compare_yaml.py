import yaml
import argparse

# This script is util to help writing of release note. It can compare two
# artifact files api.yaml from different pipeline and list their differencies.

def load_yaml(file_path):
    """Loads YAML file and creates dictionnary."""
    with open(file_path, 'r', encoding='utf-8') as file:
        return yaml.safe_load(file)

def compare_dictionnaries(dict1, dict2, path=""):
    """Compare two dictionnaries and detect missing entries in each other."""
    differences = []

    for key in dict1:
        if key not in dict2:
            differences.append(f"{path}/{key} only in YAML 1.")
        else:
            if isinstance(dict1[key], dict) and isinstance(dict2[key], dict):
                differences.extend(compare_dictionnaries(dict1[key], dict2[key], path=f"{path}/{key}"))
            elif isinstance(dict1[key], list) and isinstance(dict2[key], list):
                missing_in_dict2 = set(dict1[key]) - set(dict2[key])
                missing_in_dict1 = set(dict2[key]) - set(dict1[key])
                if missing_in_dict2:
                    differences.append(f"{path}/{key}/{missing_in_dict2} only in YAML 1.")
                if missing_in_dict1:
                    differences.append(f"{path}/{key}/{missing_in_dict1} only in YAML 2.")
            elif dict1[key] != dict2[key]:
                differences.append(f"{path}/{key} has different values: {dict1[key]} vs {dict2[key]}.")

    for key in dict2:
        if key not in dict1:
            differences.append(f"{path}/{key} only in YAML 2.")

    return differences

def main():
    parser = argparse.ArgumentParser(description="Compares 2 yaml files and displays their differencies.")
    parser.add_argument("file1", help="Path to first YAML.")
    parser.add_argument("file2", help="Path to second YAML.")
    args = parser.parse_args()

    yaml1 = load_yaml(args.file1)
    yaml2 = load_yaml(args.file2)

    differences = compare_dictionnaries(yaml1, yaml2)

    if differences:
        print("Differencies found:")
        for diff in differences:
            print(diff)
    else:
        print("Dictionnaries from files are identical.")

if __name__ == "__main__":
    main()

