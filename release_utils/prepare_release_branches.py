import os
import sys
import subprocess
import yaml

def run_command(command, cwd=None):
    """Run shell command and plot its result."""
    try:
        result = subprocess.run(command, cwd=cwd, shell=True, check=True, text=True, capture_output=True)
        print(result.stdout)
    except subprocess.CalledProcessError as e:
        print(f"Erreur lors de l'exécution de la commande : {command}")
        print(e.stderr)
        sys.exit(1)

def create_branches_in_repo(repo_name, in_tag, new_branch_name):
    os.chdir(repo_name)
    run_command(f"git checkout {in_tag}")
    print(f"Create new branch '{new_branch_name}' in '{repo_name}' from tag '{in_tag}'")
    run_command(f"git checkout -b {new_branch_name}")
    #print(f"Push '{new_branch_name}' in '{repo_name}'")
    #run_command(f"git push origin {new_branch_name}")

    # Manage submodules.
    print(f"Handle submodules...")
    run_command(f"git submodule sync --recursive && git submodule update --init --recursive")
    run_command(f"git submodule foreach --recursive 'git checkout -b {new_branch_name}'")
    #run_command(f"git submodule foreach --recursive 'git push origin {new_branch_name}'")

    # Exit.
    os.chdir("..")


def main(in_tag, new_branch_name):
    os.makedirs("codebase")
    os.chdir("codebase")

    # Clone amr_v4.
    print(f"Clone main repo git@gitlab.com:ifollow-robotics/v4/amr_v4.git...")
    run_command(f"git clone git@gitlab.com:ifollow-robotics/v4/amr_v4.git")

    # Create branches.
    create_branches_in_repo("amr_v4", in_tag, new_branch_name)

    # Clone other projects.
    print(f"Clone devops repo git@gitlab.com:ifollow-robotics/devops.git...")
    run_command(f"git clone git@gitlab.com:ifollow-robotics/devops.git")

    print(f"Clone docker repo git@gitlab.com:ifollow-robotics/dev/docker.git...")
    run_command(f"git clone git@gitlab.com:ifollow-robotics/dev/docker.git")

    print(f"Clone generic env repo git@gitlab.com:ifollow-robotics/common/envs/generic.git...")
    run_command(f"git clone git@gitlab.com:ifollow-robotics/common/envs/generic.git")

    # Create branches.
    create_branches_in_repo("devops", in_tag, new_branch_name)
    create_branches_in_repo("docker", in_tag, new_branch_name)
    create_branches_in_repo("generic", in_tag, new_branch_name)


    # In devops project, retrieve downstream projects tagged by the pipeline
    # of amr_v4 project.
    tagged_imgs = []
    with open('devops/config/images_to_tag.yaml', 'r') as file:
        tagged_imgs = yaml.safe_load(file)['images']

    for img in tagged_imgs:
        url = img.replace('registry.', '')
        url = url.replace('/', ':', 1)
        url = f"git@{url}.git"
        print(f"Clone main repo '{url}'...")
        run_command(f"git clone '{url}'")
        # Create branches.
        repo_name = url.split('/')[-1].replace('.git', '')
        create_branches_in_repo(repo_name, in_tag, new_branch_name)

    os.chdir("..")

if __name__ == "__main__":
    if len(sys.argv) != 3:
        print("Usage : python3 prepare_release_branches.py <tag> <new_branch_name>")
        sys.exit(1)

    in_tag = sys.argv[1]
    new_branch_name = sys.argv[2]

    main(in_tag, new_branch_name)

